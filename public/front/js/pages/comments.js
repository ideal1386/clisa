$('#comments-form').on('submit', function(e) {
    e.preventDefault();
    var form = $(this);
    var btn = $('.comment-submit-btn');

    var formData = new FormData(this);

    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: formData,
        success: function(data) {
            toastr.success('Your comment has been successfully sent', 'success', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });

            form.trigger('reset');
            newCaptcha();
        },

        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            block(btn);
        },
        complete: function() {
            unblock(btn);
        },

        cache: false,
        contentType: false,
        processData: false
    });

});