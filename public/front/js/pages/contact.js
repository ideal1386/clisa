(function($) {
    "use strict";

    function initialize() {
        var mapProp = {
            center: myLatlng,
            zoom: 16,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };

        var map = new google.maps.Map(document.getElementById("map"), mapProp);

        var marker = new google.maps.Marker({
            position: myLatlng,
            // icon: '/front/images/map-marker.png'
        });

        var infowindow = new google.maps.InfoWindow({
            content: text
        });

        infowindow.open(map, marker);

        marker.setMap(map);
    }

    if ($('#map').length > 0 && typeof google !== 'undefined') {
        google.maps.event.addDomListener(window, 'load', initialize);
    }
})(jQuery);

$('#contact-form').submit(function(e) {
    e.preventDefault();

    var formData = new FormData(this);
    var form = $(this);

    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        success: function(data) {
            toastr.success('Your message has been successfully sent', 'success', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
            $('#contact-form').trigger('reset');
            newCaptcha();
        },

        beforeSend: function(xhr) {
            block(form);
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        },
        complete: function() {
            unblock(form);
        },

        cache: false,
        contentType: false,
        processData: false
    });


});