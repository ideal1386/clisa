(function($) {
    $.fn.menumaker = function(options) {
        var cssmenu = $(this),
            settings = $.extend({
                format: "dropdown",
                sticky: false
            }, options);
        return this.each(function() {
            $(this).find(".button").on('click', function() {
                $(this).toggleClass('menu-opened');
                var mainmenu = $(this).next('ul');
                if (mainmenu.hasClass('open')) {
                    mainmenu.slideToggle().removeClass('open');
                } else {
                    mainmenu.slideToggle().addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });
            cssmenu.find('li ul').parent().addClass('has-sub');
            multiTg = function() {
                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                cssmenu.find('.submenu-button').on('click', function() {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').slideToggle();
                    } else {
                        $(this).siblings('ul').addClass('open').slideToggle();
                    }
                });
            };
            if (settings.format === 'multitoggle') multiTg();
            else cssmenu.addClass('dropdown');
            if (settings.sticky === true) cssmenu.css('position', 'fixed');
            resizeFix = function() {
                var mediasize = 1000;
                if ($(window).width() > mediasize) {
                    cssmenu.find('ul').show();
                }
                if ($(window).width() <= mediasize) {
                    cssmenu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);
        });
    };
})(jQuery);

(function($) {
    $(document).ready(function() {
        $("#cssmenu").menumaker({
            format: "multitoggle"
        });
    });
})(jQuery);

function block(el) {

    var block_ele = $(el);

    // Block Element
    block_ele.block({
        message: '<div class="fa fa-repeat fa-spin text-primary"></div>',
        overlayCSS: {
            backgroundColor: "#fff",
            cursor: "wait"
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: "none"
        }
    });
}

function unblock(el) {
    $(el).unblock();
}

$.ajaxSetup({
    error: function(data) {

        if (data.status == 403) {
            toastr.error('403', 'error', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
            return;

        } else if (!data.responseJSON.errors) {
            toastr.error('An error has occurred', 'error', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
            return;
        }

        for (var key in data.responseJSON.errors) {
            // skip loop if the property is from prototype
            if (!data.responseJSON.errors.hasOwnProperty(key)) continue;

            var obj = data.responseJSON.errors[key];
            for (var prop in obj) {
                // skip loop if the property is from prototype
                if (!obj.hasOwnProperty(prop)) continue;

                toastr.error(obj[prop], 'error', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });

            }
        }

    },
});

$('img.captcha').on('click', function() {
    newCaptcha();
});

function newCaptcha() {
    $.ajax({
        url: BASE_URL + "/get-new-captcha",
        type: 'GET',
        data: {},
        success: function(data) {
            $('img.captcha').attr('src', data.captcha);
        },
    });
}

$('#newsletter-create-form').submit(function(e) {
    e.preventDefault();

    var formData = new FormData(this);
    var form = $(this);

    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        success: function(data) {

            toastr.success('You have successfully subscribed to our newsletter', 'success', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });

            form.trigger('reset');
        },

        beforeSend: function(xhr) {
            block(form);
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        },
        complete: function() {
            unblock(form);
        },

        cache: false,
        contentType: false,
        processData: false
    });

});

$(document).ready(function() {
    newCaptcha();
});