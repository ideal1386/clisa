
$(document).on('click', '.btn-delete', function () {
    $('#video-delete-form').attr('action', '/admin/videos/' + $(this).data('video'));
    $('#video-delete-form').data('id', $(this).data('id'));
});

$('#video-delete-form').submit(function (e) {
    e.preventDefault();

    $('#delete-modal').modal('hide');

    var form = this;

    var formData = new FormData(this);

    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        success: function (data) {
            //get current url
            var url = window.location.href;

            //remove video tr
            $('#video-' + $(form).data('id') + '-tr').remove();

            toastr.success('پست با موفقیت حذف شد.');

            //refresh videos list
            $(".app-content").load(url + " .app-content > *");
        },
        beforeSend: function (xhr) {
            block('#main-card');
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        },
        complete: function () {
            unblock('#main-card');
        },
        cache: false,
        contentType: false,
        processData: false
    });


});