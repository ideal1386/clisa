/*=========================================================================================
    File Name: settings.js
    Description: settings page
==========================================================================================*/

$(document).ready(function() {


    /*=========+===================
      Profile Tab Js Codes
    ===============================*/

    // validate form with jquery validation plugin
    jQuery('#profile-form').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').find('input').removeClass('is-invalid').addClass('is-invalid');
        },
        success: function(e) {
            jQuery(e).closest('.form-group').find('input').removeClass('is-invalid');
            jQuery(e).remove();
        },
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 150
            }, 200);

            $(validator.errorList[0].element).focus();

        },
        rules: {
            'first_name': {
                required: true,
            },
            'last_name': {
                required: true,
            },
            'username': {
                required: true,
                maxlength: 191,
            },
            'password': {
                minlength: 6,
            },
            'password_confirmation': {
                equalTo: "#password"
            },
        },
        messages: {
            'first_name': {
                required: 'لطفا نام خودتان را وارد کنید',
            },
            'last_name': {
                required: 'لطفا نام خانوادگی خودتان را وارد کنید',
            },
            'username': {
                required: 'لطفا نام کاربری را وارد کنید',
            },
            'password': {
                minlength: 'گذرواژه باید حداقل 6 کاراکتر باشد',
            },
            'password_confirmation': {
                equalTo: 'تکرار گذرواژه با گذرواژه برابر نیست',
            },

        }
    });

    $('#edit-image-btn').click(function() {
        $('#profile-image').trigger('click');
    });

    $('#profile-image').change(function() {
        if (this.files && this.files[0]) {

            var FR = new FileReader();

            FR.addEventListener("load", function(e) {
                document.getElementById("profile-pic").src = e.target.result;
            });

            FR.readAsDataURL(this.files[0]);
        }
    });

    $('#profile-form').submit(function(e) {
        e.preventDefault();

        if ($(this).valid()) {
            var formData = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                success: function(data) {
                    Swal.fire({
                        type: 'success',
                        title: 'تغییرات با موفقیت ذخیره شد',
                        confirmButtonClass: 'btn btn-primary',
                        confirmButtonText: 'باشه',
                        buttonsStyling: false,
                    })
                },
                beforeSend: function(xhr) {
                    block('#main-card');
                    xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                complete: function() {
                    unblock('#main-card');
                },

                cache: false,
                contentType: false,
                processData: false
            });
        }

    });

    /*=========+===================
      Information Tab Js Codes
    ===============================*/

    $('#tags').tagsInput({
        'defaultText': 'افزودن',
        'width': '100%',
    });

    $('#province, #city').select2({
        rtl: true,
        width: '100%',
    });

    // validate form with jquery validation plugin
    jQuery('#information-form').validate({
        errorClass: 'invalid-feedback animated fadeInDown',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').find('input').removeClass('is-invalid').addClass('is-invalid');
        },
        success: function(e) {
            jQuery(e).closest('.form-group').find('input').removeClass('is-invalid');
            jQuery(e).remove();
        },
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top - 150
            }, 200);

            $(validator.errorList[0].element).focus();

        },
        rules: {
            'info_site_title': {
                required: true,
            },

        },
        messages: {
            'info_site_title': {
                required: 'لطفا عنوان وبسایت را وارد کنید',
            },

        }
    });

    $('#province').change(function() {
        var id = $(this).find(":selected").val();
        $('#city').empty();

        $.ajax({
            type: 'get',
            url: '/province/get-cities',
            data: { id: id },
            success: function(data) {
                $(data).each(function() {
                    $('#city').append('<option value="' + $(this)[0].id + '">' + $(this)[0].name + '</option>')
                });
            },
            beforeSend: function() {
                block('#city-div');
            },
            complete: function() {
                unblock('#city-div');
            },

        });

    });

    $('#information-form').submit(function(e) {
        e.preventDefault();

        if ($(this).valid()) {
            var formData = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                success: function(data) {
                    Swal.fire({
                        type: 'success',
                        title: 'تغییرات با موفقیت ذخیره شد',
                        confirmButtonClass: 'btn btn-primary',
                        confirmButtonText: 'باشه',
                        buttonsStyling: false,
                    })
                },
                beforeSend: function(xhr) {
                    block('#main-card');
                    xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                complete: function() {
                    unblock('#main-card');
                },

                cache: false,
                contentType: false,
                processData: false
            });
        }

    });

    /*=========+===================
      Socials Tab Js Codes
    ===============================*/

    $('#socials-form, #sizes-form').submit(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            success: function(data) {
                Swal.fire({
                    type: 'success',
                    title: 'تغییرات با موفقیت ذخیره شد',
                    confirmButtonClass: 'btn btn-primary',
                    confirmButtonText: 'باشه',
                    buttonsStyling: false,
                })
            },
            beforeSend: function(xhr) {
                block('#main-card');
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            },
            complete: function() {
                unblock('#main-card');
            },

            cache: false,
            contentType: false,
            processData: false
        });

    });

    /*=========+===================
      Gateway Tab Js Codes
    ===============================*/

    $('#mellat').change(function() {
        if ($(this).prop('checked')) {
            $('.mellat').prop('disabled', false);
        } else {
            $('.mellat').prop('disabled', true);
        }
    });

    $('#payir').change(function() {
        if ($(this).prop('checked')) {
            $('.payir').prop('disabled', false);
        } else {
            $('.payir').prop('disabled', true);
        }
    });

    $('#mellat').trigger('change');
    $('#payir').trigger('change');

    // validate form with jquery validation plugin
    jQuery('#gateway-form').validate();

    $('#gateway-form').submit(function(e) {
        e.preventDefault();

        if ($(this).valid()) {
            var formData = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                success: function(data) {
                    Swal.fire({
                        type: 'success',
                        title: 'تغییرات با موفقیت ذخیره شد',
                        confirmButtonClass: 'btn btn-primary',
                        confirmButtonText: 'باشه',
                        buttonsStyling: false,
                    })
                },
                beforeSend: function(xhr) {
                    block('#main-card');
                    xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                complete: function() {
                    unblock('#main-card');
                },

                cache: false,
                contentType: false,
                processData: false
            });
        }

    });

});


// map js codes

var map;
var gmarkers = [];

function initialize() {
    var mapOptions = {
        zoom: 16,
        center: myLatlng,
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    placeMyMarker()
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
    });
}

function placeMarker(location) {
    removeMarkers();
    var marker = new google.maps.Marker({
        position: location,
        map: map,
    });
    var infowindow = new google.maps.InfoWindow({
        content: '<div style="direction: rtl; text-align: right;">' + '<h4>موقعیت شما</h4>' + '</div>'
    });
    infowindow.open(map, marker);

    $('#Longitude').val(location.lng());
    $('#latitude').val(location.lat());

    gmarkers.push(marker);
}

function placeMyMarker() {
    removeMarkers();
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
    });
    var infowindow = new google.maps.InfoWindow({
        content: '<div style="direction: rtl; text-align: right;">' + '<h4>موقعیت شما</h4>' + '</div>'
    });

    infowindow.open(map, marker);
    gmarkers.push(marker);
}

function removeMarkers() {
    for (i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setMap(null);
    }
}
if (typeof google !== 'undefined')
    google.maps.event.addDomListener(window, 'load', initialize);