$(document).on('click', '.btn-delete', function() {
    $('#newsletter-delete-form').attr('action', '/admin/newsletters/' + $(this).data('newsletter'));
    $('#newsletter-delete-form').data('id', $(this).data('newsletter'));
});

$('#newsletter-delete-form').submit(function(e) {
    e.preventDefault();

    $('#delete-modal').modal('hide');

    var formData = new FormData(this);

    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        success: function(data) {
            //get current url
            var url = window.location.href;

            //remove newsletter tr
            $('#newsletter-' + $('#newsletter-delete-form').data('id') + '-tr').remove();

            toastr.success('پیام با موفقیت حذف شد.');

            //refresh newsletters list
            $(".app-content").load(url + " .app-content > *");
        },
        beforeSend: function(xhr) {
            block('#main-card');
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        },
        complete: function() {
            unblock('#main-card');
        },
        cache: false,
        contentType: false,
        processData: false
    });


});