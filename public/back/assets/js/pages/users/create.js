

// validate form with jquery validation plugin
jQuery('#user-create-form').validate({
    errorClass: 'invalid-feedback animated fadeInDown',
    errorPlacement: function(error, e) {
        jQuery(e).parents('.form-group').append(error);
    },
    highlight: function(e) {
        jQuery(e).closest('.form-group').find('input').removeClass('is-invalid').addClass('is-invalid');
    },
    success: function(e) {
        jQuery(e).closest('.form-group').find('input').removeClass('is-invalid');
        jQuery(e).remove();
    },
    invalidHandler: function(form, validator) {

        if (!validator.numberOfInvalids())
            return;

        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top - 150
        }, 200);

        $(validator.errorList[0].element).focus();

    },
    rules: {
        'first_name': {
            required: true,
        },
        'last_name': {
            required: true,
        },

        'username': {
            required: true,
            // regex: "(09)[0-9]{9}"
        },

        'password': {
            required: true,
            minlength: 8
        },

        'password_confirmation': {
            required: true,
            equalTo: "#password"
        },

    },
});
jQuery('#user-edit-form').validate({
    errorClass: 'invalid-feedback animated fadeInDown',
    errorPlacement: function(error, e) {
        jQuery(e).parents('.form-group').append(error);
    },
    highlight: function(e) {
        jQuery(e).closest('.form-group').find('input').removeClass('is-invalid').addClass('is-invalid');
    },
    success: function(e) {
        jQuery(e).closest('.form-group').find('input').removeClass('is-invalid');
        jQuery(e).remove();
    },
    invalidHandler: function(form, validator) {

        if (!validator.numberOfInvalids())
            return;

        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top - 150
        }, 200);

        $(validator.errorList[0].element).focus();

    },
    rules: {
        'first_name': {
            required: true,
        },
        'last_name': {
            required: true,
        },

        'username': {
            required: true,
            // regex: "(09)[0-9]{9}"
        },

        'password_confirmation': {
            equalTo: "#password"
        },

    },
});

$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "لطفا یک مقدار معتبر وارد کنید"
);

$('#user-create-form, #user-edit-form').submit(function(e) {
    e.preventDefault();
    var form = $(this);

    if ($(this).valid() && !$(this).data('disabled')) {
        var formData = new FormData(this);

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            success: function(data) {
                form.data('disabled', true);
                window.location.href = BASE_URL + "/users";
            },
            beforeSend: function(xhr) {
                block('#main-card');
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            },
            complete: function() {
                unblock('#main-card');
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

});