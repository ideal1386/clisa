/*=========================================================================================
    File Name: login.js
    Description: login page
==========================================================================================*/

$(document).ready(function () {

    $('#login-form').submit(function (e) {
        e.preventDefault();
        

        var formData = new FormData(this);
            
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    window.location.href = redirect_url;
                }
            },
            error: function(data){
                
                var errors = data.responseJSON;

                if(!errors) {
                    toastr.error('خطایی رخ داده است', 'خطا', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
                    return;
                }

                for (var key in data.responseJSON.errors) {
                    // skip loop if the property is from prototype
                    if (!data.responseJSON.errors.hasOwnProperty(key)) continue;

                    var obj = data.responseJSON.errors[key];
                    for (var prop in obj) {
                        // skip loop if the property is from prototype
                        if (!obj.hasOwnProperty(prop)) continue;

                        toastr.error(obj[prop], 'خطا', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });

                    }
                }     

            },
            beforeSend: function (xhr) {
                block('#main-card');
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            },
            complete: function() {
                unblock('#main-card');
            },

            cache: false,
            contentType: false,
            processData: false
        });

    });

});