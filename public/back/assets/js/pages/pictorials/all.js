CKEDITOR.config.height = 400;
CKEDITOR.replace('content');

$('.tags').tagsInput({
    'defaultText': 'افزودن',
    'width': '100%',
    'autocomplete_url': '/admin/get-tags',
});

$('.pictorial-category').select2({
    rtl: true,
    width: '100%',
});

// validate form with jquery validation plugin
jQuery('#pictorial-create-form, #pictorial-edit-form').validate({

    rules: {
        'title': {
            required: true,
        },
        'price': {
            required: true,
            digits: true,
            min: 100,
            max: 50000000

        },
        'discount': {
            digits: true,
            max: 100

        },
        'weight': {
            required: true,
            digits: true

        },
        'stock': {
            required: true,
            digits: true

        },

    },
});