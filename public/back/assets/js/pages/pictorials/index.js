
$(document).on('click', '.btn-delete', function () {
    $('#pictorial-delete-form').attr('action', '/admin/pictorials/' + $(this).data('pictorial'));
    $('#pictorial-delete-form').data('id', $(this).data('id'));
});

$('.pictorial-category').select2({
    rtl: true,
    width: '100%',
    placeholder: "انتخاب کنید",
});

$('#pictorial-delete-form').on('submit', function (e) {
    e.preventDefault();

    $('#delete-modal').modal('hide');

    var formData = new FormData(this);

    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        success: function (data) {
            //get current url
            var url = window.location.href;

            //remove pictorial tr
            $('#pictorial-' + $(this).data('id') + '-tr').remove();

            toastr.success('به روایت تصویر با موفقیت حذف شد.');

            //refresh pictorials list
            $(".pictorials").load(url + " .pictorials > *");
        },
        beforeSend: function (xhr) {
            block('#main-card');
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        },
        complete: function () {
            unblock('#main-card');
        },
        cache: false,
        contentType: false,
        processData: false
    });


});