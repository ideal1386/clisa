function block(el) {
    var $body = $("body");

    var block_ele = $(el);

    var reloadActionOverlay;
    if ($body.hasClass("dark-layout")) {
        var reloadActionOverlay = "#10163a";
    } else {
        var reloadActionOverlay = "#fff";
    }

    // Block Element
    block_ele.block({
        message: '<div class="feather icon-refresh-cw icon-spin font-medium-2 text-primary"></div>',
        overlayCSS: {
            backgroundColor: reloadActionOverlay,
            cursor: "wait"
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: "none"
        }
    });
}

function unblock(el) {
    $(el).unblock();
}

$.ajaxSetup({
    error: function(data) {

        console.log(data)
        if (data.status == 403) {
            toastr.error('اجازه ی دسترسی ندارید', 'خطا', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
            return;

        } else if (!data.responseJSON.errors) {
            toastr.error('خطایی رخ داده است', 'خطا', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
            return;
        }

        for (var key in data.responseJSON.errors) {
            // skip loop if the property is from prototype
            if (!data.responseJSON.errors.hasOwnProperty(key)) continue;

            var obj = data.responseJSON.errors[key];
            for (var prop in obj) {
                // skip loop if the property is from prototype
                if (!obj.hasOwnProperty(prop)) continue;

                toastr.error(obj[prop], 'خطا', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });

            }
        }

    },
});


function reloadDiv(el) {
    //get current url
    var url = window.location.href;

    console.log(el);
    //refresh comments list
    $(el).load(url + " " + el + " > *");
}