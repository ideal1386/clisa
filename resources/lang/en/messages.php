<?php

return [

    'socials' => [
        'instagram' => 'instagram',
        'facebook' => 'facebook',
        'twitter' => 'twitter',
        'youtube' => 'youtube',
        'telegram' => 'telegram',
        'tiktok' => 'tiktok',
        'whatsup' => 'whatsup',
    ],

    'footer' => [
        'latest-gallaries' => 'Latest Gallaries',
        'newsletter' => 'Newsletter:',
        'submit-newsletter' => 'Submit',
        'subscribe-to-newsletter' => 'subscribe to out newsletter',
        'form-name' => 'Name',
        'form-email' => 'Email',
        'form-captcha' => 'captcha',
    ],

    'sidebar' => [
        'latest-posts' => 'last posts',
        'latest-videos' => 'last videos',
    ],

    'posts' => [
        'blog' => 'blog',
        'read-more' => 'Read More',
    ],

    'videos' => [
        'view-more' => 'View More',
        'videos' => 'Videos',
    ],

    'pictorials' => [
        'view-more' => 'View More',
        'gallery' => 'Gallery',
    ],

    'page' => [
        'read-more' => 'Read More',
        'blog' => 'blog',
    ],

    'comments' => [
        'comments' => 'Comments',
        'write-new-comment' => 'Write New Message',
        'name' => 'Name :',
        'email' => 'Email :',
        'message' => 'Message :',
        'send' => 'Send',
    ],

    'pages' => [
        'contact' => [
            'contact-info'        => 'contact info',
            'contact-form-header' => 'Make your suggestions or complaints',
            'name-input'          => 'fullname',
            'email-input'         => 'email',
            'subject-input'       => 'subject',
            'message-input'       => 'your message',
            'submit-btn'          => 'send',
            'our-location'        => 'our location',
            'captcha'             => 'captcha code'
        ],

        'index' => [
            'read-more' => ' Read more',
            'our-blog'   => 'OUR BLOG',
            'our-videos'   => 'OUR VIDEOS',
        ],

        'videos' => [
            'more-videos'    => 'more videos',
            'see-all-videos' => 'see all videos',
        ],

        '404' => [
            '404-page-not-found' => '404 Page not found',
            'sorry-page-not-found' => 'Sorry this page not found',
            'back-to-home' => 'Back to home page'
        ],
        
        '500' => [
            'server-error' => 'Server Error',
            'an-error-occurred' => 'An Error occurred',
            'back-to-home' => 'Back to home page'
        ],

        '403' => [
            'not-allowed' => 'Not Allowed',
            'not-allowed-to-this-page' => 'you are not allowed to access this page',
            'back-to-home' => 'Back to home page'
        ]
    ],
    
    'contact' => [
        'contact' => 'Contact',
        'name' => 'Name',
        'name-placeholder' => 'Your Name',
        'subject' => 'Subject',
        'email-address' => 'Email address',
        'message' => 'Message',
        'captcha' => 'Captcha',
        'send' => 'Send',
        'our-location' => 'our location',
    ]
];
