<?php

return [

    'socials' => [
        'instagram' => 'اینستاگرام',
        'facebook' => 'فیسبوک',
        'twitter' => 'تویتر',
        'youtube' => 'یوتیوب',
        'telegram' => 'تلگرام',
        'tiktok' => 'تیک تاک',
        'whatsup' => 'واتساپ',
    ],

    'footer' => [
        'latest-gallaries' => 'آخرین گالری تصاویر',
        'newsletter' => 'خبرنامه:',
        'submit-newsletter' => 'ارسال',
        'subscribe-to-newsletter' => 'عضویت در خبرنامه ما',
        'form-name' => 'نام',
        'form-email' => 'ایمیل',
        'form-captcha' => 'کد امنیتی',
    ],

    'sidebar' => [
        'latest-posts' => 'آخرین پست ها',
        'latest-videos' => 'آخرین ویدیوها',
    ],

    'posts' => [
        'blog' => 'وبلاگ',
        'read-more' => 'بیشتر بخوانید',
    ],

    'videos' => [
        'view-more' => 'مشاهده بیشتر',
        'videos' => 'ویدیوها',
    ],

    'pictorials' => [
        'view-more' => 'مشاهده بیشتر',
        'gallery' => 'گالری',
    ],

    'page' => [
        'read-more' => 'بیشتر بخوانید',
        'blog' => 'وبلاگ',
    ],

    'comments' => [
        'comments' => 'دیدگاه ها',
        'write-new-comment' => 'ارسال دیدگاه جدید',
        'name' => 'نام :',
        'email' => 'ایمیل :',
        'message' => 'پیام :',
        'send' => 'ارسال',
    ],

    'pages' => [
        'contact' => [
            'contact-info'        => 'اطلاعات تماس',
            'contact-form-header' => 'پیشنهادات یا شکایات خود را مطرح کنید',
            'name-input'          => 'نام شما',
            'email-input'         => 'ایمیل',
            'subject-input'       => 'موضوع',
            'message-input'       => 'پیام شما',
            'submit-btn'          => 'ارسال',
            'our-location'        => 'موقعیت ما',
            'captcha'             => 'کد امنیتی'
        ],

        'index' => [
            'read-more' => ' بیشتر بخوانید',
            'our-blog'   => 'وبلاگ',
            'our-videos'   => 'ویدیو ها',
        ],

        'videos' => [
            'more-videos'    => 'ویدیو های بیشتر',
            'see-all-videos' => 'همه ویدیوها',
        ],

        '404' => [
            '404-page-not-found' => '404 صفحه یافت نشد',
            'sorry-page-not-found' => 'متاسفانه صفحه مورد نظر شما پیدا نشد',
            'back-to-home' => 'بازگشت به صفحه اصلی'
        ],
        
        '500' => [
            'server-error' => 'خطای سرور',
            'an-error-occurred' => 'خطایی رخ داده است',
            'back-to-home' => 'بازگشت به صفحه اصلی'
        ],

        '403' => [
            'not-allowed' => 'اجازه دسترسی ندارید',
            'not-allowed-to-this-page' => 'شما اجازه دسترسی به این صفحه را ندارید',
            'back-to-home' => 'بازگشت به صفحه اصلی'
        ]
    ],
    
    'contact' => [
        'contact' => 'تماس با ما',
        'name' => 'نام',
        'name-placeholder' => 'نام شما',
        'subject' => 'موضوع',
        'email-address' => 'ایمیل',
        'message' => 'پیام',
        'captcha' => 'کد امنیتی',
        'send' => 'ارسال',
        'our-location' => 'موقعیت ما',
    ]
];
