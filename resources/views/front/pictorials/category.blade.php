@extends('front.layouts.master')

@section('content')
    <div class="container">
        <h3>{{ $category->title }}</h3>
        <div class="row">
            @foreach ($pictorials as $pictorial)
                <div class="col-sm-6 col-md-4 col-lg-3 my-2">
                    <div class="postv">
                        <div class="post-img">
                            <img src="{{ $pictorial->image ? asset($pictorial->image) : asset('front/img/no-image.jpg') }}" alt="{{ $pictorial->title }}">
                        </div>
                        <div class="post-title pt-3 pl-2">
                            <h5 class="text-info">{{ $pictorial->title }}</h5>
                        </div>
                        <div class="post-sum">
                            <p>
                                {!! $pictorial->shortContent(30) !!}
                            </p>
                            <a class="btn btn-warning" href="{{ route('front.pictorials.show', ['pictorial' => $pictorial]) }}">{{ __('messages.pictorials.view-more') }}</a>
                        </div>
                    </div>
                </div>    
            @endforeach
        </div>
    </div>
@endsection
