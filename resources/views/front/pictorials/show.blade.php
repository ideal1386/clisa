@extends('front.layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('front/css/lightgallery.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/gallery.css') }}">
@endpush

@section('content')
    <main class="container-fluid mt-5">
        <!-- main body -->
        <div class="content">
            <div class="bread-tap">
                <div class="bread-camp">
                    <span class="mr-2">{{ __('messages.pictorials.gallery') }}</span>
                    <span class=" fas fa-angle-right mr-2"></span>
                    <span>{{ $pictorial->category ? $pictorial->category->title : '' }}</span>
                </div>
            </div>
            <div class="cont">
                <div class="album container">
                    <h3 class="my-3">{{ $pictorial->title }}</h3>
                </div>
                <div class="demo-gallery">
                    <ul id="lightgallery">
                        @foreach ($pictorial->gallery as $image)
                            <li data-src="{{ asset($image->image) }}">
                                <a href="">
                                    <img class="img-responsive" src="{{ asset($image->image) }}" alt="image">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="container">
                {!! $pictorial->content !!}
            </div>

            <div class="container">
                @include('front.components.comments', ['model' => $pictorial, 'route_link' => route('front.pictorial.comments', ['pictorial' => $pictorial]) ])
            </div>

        </div>
        <!-- / main body -->
        <div class="clear"></div>
    </main>
@endsection

@push('scripts')
    <script src="{{ asset('front/js/lightgallery-all.min.js') }}"></script>
    <script src="{{ asset('front/js/custom.js') }}"></script>
@endpush
