@extends('front.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm 12 col-9">
                @if ($post->image)
                    <div class="post-pic">
                        <img src="{{ asset($post->image) }}" alt="{{ $post->title }}">
                    </div>
                @endif
                <div class="title">
                    <h3 class="my-3">
                        {{ $post->title }}
                    </h3>
                </div>
                <div class="body text-justify px-3">
                    {!! $post->content !!}
                </div>

                @include('front.components.comments', ['model' => $post, 'route_link' => route('front.post.comments', ['post' => $post]) ])
            </div>

            @include('front.partials.sidebar')
        </div>
    </div>
@endsection