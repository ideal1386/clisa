@extends('front.layouts.master')

@section('content')
    

    @if ($main_sliders->count())
        <div class="container-fluid px-0">
            <div id="Church" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach ($main_sliders as $slider)
                        <li data-target="#Church" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach ($main_sliders as $slider)
                        <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                            <img class="d-block w-100" src="{{ asset($slider->image) }}">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slider->title }}</h5>
                                <p>{{ $slider->description }}</p>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
                <a class="carousel-control-prev" href="#Church" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#Church" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    @endif
    
    <div class="wrapper row3">

        @if ($service_sliders->count())

            <main class="container">
                <div class="statics">
                    @foreach ($service_sliders as $slider)
                        <div>
                            <a href="{{ $slider->link }}">
                                <div class="statics-section mb-3">
                                    <div>
                                        <img class="statics-img" src="{{ asset($slider->image) }}" alt="{{ $slider->title }}">
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <p class="statics-text">{{ $slider->title }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </main>

        @endif
       

        @if ($latest_pictorials->count())
            <div class="top-article">
                <div class="container top-article-section">
                    @foreach ($latest_pictorials as $pictorial)
                        <div class="col-md-4">
                            <div class="article-card my-2">
                                <div class="aticle-img">
                                    <a href="{{ route('front.pictorials.show', ['pictorial' => $pictorial]) }}">
                                        <img src="{{ $pictorial->image ? asset($pictorial->image) : asset('front/img/no-image.jpg') }}" alt="{{ $pictorial->title }}">
                                    </a>
                                </div>
                                <div class="article-txt">
                                    <h4>{{ $pictorial->title }}</h4>
                                    <p>
                                        {!! $pictorial->shortContent(20) !!}
                                    </p>
                                    <a class="btn btn-warning" href="{{ route('front.pictorials.show', ['pictorial' => $pictorial]) }}">{{ __('messages.pages.index.read-more') }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

    </div>

    @if ($latest_posts->count())
        <div class="blog-back">
            <div class="blog-section container">
                <div class="blog-header">
                    <h2>{{ __('messages.pages.index.our-blog') }}</h2>
                    <hr>
                    <p>
                        {!! nl2br(option('index_blog_text')) !!}
                    </p>
                </div>
                <div class="blog-card">
                    @foreach ($latest_posts as $post)
                        <div class="col-md-4">
                            <div class="article-card my-2">
                                <div class="aticle-img">
                                    <a href="{{ route('front.posts.show', ['post' => $post]) }}">
                                        <img src="{{ $post->image ? asset($post->image) : asset('front/img/no-image.jpg') }}" alt="{{ $post->title }}">                            </div>
                                    </a>
                                <div class="article-txt">
                                    <h4>{{ $post->title }}</h4>
                                    <p>
                                        {!! $post->shortContent(30) !!}
                                    </p>
                                    <a class="btn btn-warning" href="{{ route('front.posts.show', ['post' => $post]) }}">{{ __('messages.pages.index.read-more') }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if ($latest_videos->count())
        <div class="video-bar">
            <div class="video-bar-header">
                <h2><span>{{ __('messages.pages.index.our-videos') }}</span></h2>
            </div>
            <div class="video-exm container">
                <div class="row justify-content-center">
                    @foreach ($latest_videos as $video)
                        <div class="col-md-4">
                            <a href="{{ route('front.videos.show', ['video' => $video]) }}" target="_blank">
                                <div class="video-pev mb-3">
                                    <img src="{{ $video->image ? asset($video->image) : asset('front/img/no-image.jpg') }}" alt="{{ $video->title }}">
                                    <div class="vid-icon"><img class="vid-icon" src="{{ asset('front\img\vid-player.png') }}" alt="video icon"></div>
                                    
                                    <div class="vid-title">{{ $video->title }}</div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    
@endsection
