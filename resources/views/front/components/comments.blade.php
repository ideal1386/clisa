<hr>
@if($model->comments->count())
    <h3 class="mb-3">{{ __('messages.comments.comments') }}</h3>
    
    @foreach($model->comments as $comment)
        <div>
            <div class="comment">
                <div class="comment-body">
                    <p class="name text-info">{{ $comment->name() }} :</p>
                    <p class="text">{!! nl2br(htmlentities($comment->body)) !!}</p>
                </div>
            </div>

            @php
                $child_comments = $comment->comments()->where('status', 'accepted')->get()
            @endphp

            @foreach($child_comments as $child_comment)
                <div class="comment answer">
                    <div class="comment-body">
                        <p class="name text-info">{{ $child_comment->name() }} :</p>
                        <p class="text">{!! nl2br(htmlentities($child_comment->body)) !!}</p>
                    </div>
                </div>
            @endforeach

        </div>
    @endforeach

@endif

<h4 class="mt-5 text-secondary">{{ __('messages.comments.write-new-comment') }}</h4>
<div class="comments">
    <div class="send-comment">
        <form id="comments-form" action="{{ $route_link }}">
            <div class="row">
                <div class="form-group col-lg-6">
                    <label for="name">{{ __('messages.comments.name') }} </label>
                    <input name="name" type="text" class="form-control" id="name" required>
                </div>
                <div class="form-group col-lg-6">
                    <label for="email">{{ __('messages.comments.email') }} </label>
                    <input name="email" type="email" class="form-control" id="email" required>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="body">{{ __('messages.comments.message') }} </label>
                        <textarea name="body" class="form-control" id="body" rows="3" required></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3 col-6">
                    <input type="text" class="form-control" id="captcha" name="captcha" placeholder=""
                        autocomplete="off" required>
                </div>
                <div class="col--md-3 col-6">
                    <img class="captcha" src="{{ captcha_src('flat') }}" alt="captcha">
                </div>
            </div>

            <button class="btn btn-success comment-submit-btn">{{ __('messages.comments.send') }}</button>
        </form>
    </div>
</div>