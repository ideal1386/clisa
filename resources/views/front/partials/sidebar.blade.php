
<div class="col-sm-12 col-lg-3">
    <div class="sidebar border rounded p-3">
        <div class="lastpost">
            <h2 class="text-center text-muted">{{ __('messages.sidebar.latest-posts') }}</h2>
            @foreach ($latest_posts as $latest_post)
                <hr>
                <div class="mt-3">
                    <a href="{{ route('front.posts.show', ['post' => $latest_post]) }}">
                        <h5 class="text-info">{{ $latest_post->title }}</h5>
                        <h6 class="text-dark">{!! $latest_post->shortContent(5) !!}</h6>
                    </a>
                </div>
            @endforeach
        </div>

        @if ($latest_videos->count())
            <div class="mt-5">
                <h2 class="text-muted text-center">{{ __('messages.sidebar.latest-videos') }}</h2>
                <hr>
            </div>
            <div class="px-2">
                @foreach ($latest_videos as $sidebar_video)
                    <div>
                        <img class="mb-2" src="{{ $sidebar_video->image ? asset($sidebar_video->image) : asset('front/img/no-image.jpg') }}" alt="{{ $sidebar_video->title }}">
                        <a href="{{ route('front.videos.show', ['video' => $sidebar_video]) }}">
                            <h5 class="pl-3 text-dark">{{ $sidebar_video->title }}</h5>
                        </a>
                    </div>
                @endforeach
            </div>
        @endif
    </div>

</div>
