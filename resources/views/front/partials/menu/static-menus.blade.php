@switch($menu->static_type)

    @case('posts')
        @if($postcats->count())
          
            <li>
                <a href="{{ route('front.posts.index') }}">{{ $menu->title }}</a>
                <ul>
                    @foreach($postcats as $category)
                        @include('front.partials.menu.child-category', ['category' => $category])
                    @endforeach
                </ul>
            </li>
           
        @endif
        
        @break

    @case('videos')
        @if($videocats->count())
          
            <li>
                <a href="{{ route('front.videos.index') }}">{{ $menu->title }}</a>
                <ul>
                    @foreach($videocats as $category)
                        @include('front.partials.menu.child-category', ['category' => $category])
                    @endforeach
                </ul>
            </li>
           
        @endif
        
        @break

    @case('pictorials')
        @if($pictorialcats->count())

            <li>
                <a href="{{ route('front.pictorials.index') }}">{{ $menu->title }}</a>
                <ul>
                    @foreach($pictorialcats as $category)
                        @include('front.partials.menu.child-category', ['category' => $category])
                    @endforeach
                </ul>
            </li>
            
        @endif
        
        @break

@endswitch
