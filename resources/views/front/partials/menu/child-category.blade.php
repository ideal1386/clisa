
@if($category->categories->isEmpty())

    <li>
        <a href="{{ $category->link }}">{{ $category->title }}</a>
    </li>

@else
    <li>
        <a href="{{ $category->link }}">{{ $category->title }}</a>
        <ul>
            @foreach ($category->childrenCategories as $childCategory)
                @include('front.partials.menu.child-category', ['category' => $childCategory])
            @endforeach
        </ul>
    </li>
@endif
