<div class="col-lg-12 px-0 bg-yello">
     <div class="container">
      
        <header>
            <nav id='cssmenu'>
                
                <div id="head-mobile"></div>
                <div class="button">
                    <i class="fa fa-bars"></i>
                </div>
                <ul class="menu-head">
                    @foreach($menus as $menu)
                        @include('front.partials.menu.child-menu')
                    @endforeach
                </ul>
            </nav>
        </header>
    
     </div>
</div>