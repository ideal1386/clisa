<footer>
    <div class="top-footer"></div>
    <div class="wrapper row4">
        <footer id="footer" class="hoc clear">
            <div class="one_third first">
                <h6 class="heading">{{ option('info_site_title') }}</h6>
                <p>{!! nl2br(option('info_short_description')) !!}</p>

                <ul class="faico clear">
                    @if (option('social_telegram'))
                        <li><a href="{{ option('social_telegram') }}"><i class="fab fa-telegram"></i></a></li>
                    @endif

                    @if (option('social_instagram'))
                        <li><a href="{{ option('social_instagram') }}"><i class="fab fa-instagram"></i></a></li>
                    @endif

                    @if (option('social_facebook'))
                        <li><a href="{{ option('social_facebook') }}"><i class="fab fa-facebook"></i></a></li>
                    @endif

                    @if (option('social_whatsapp'))
                        <li><a href="{{ option('social_whatsapp') }}"><i class="fab fa-whatsapp"></i></a></li>
                    @endif

                    @if (option('social_twitter'))
                        <li><a href="{{ option('social_twitter') }}"><i class="fab fa-twitter"></i></a></li>
                    @endif

                    @if (option('social_youtube'))
                        <li><a href="{{ option('social_youtube') }}"><i class="fab fa-youtube"></i></a></li>
                    @endif

                    @if (option('social_linkedin'))
                        <li><a href="{{ option('social_linkedin') }}"><i class="fab fa-linkedin"></i></a></li>
                    @endif

                </ul>
            </div>

            @if ($latest_pictorials->count())
                <div class="one_third">
                    <h6 class="heading">{{ __('messages.footer.latest-gallaries') }}</h6>
                    <ul class="nospace clear latestimg">
                        @foreach ($latest_pictorials as $footer_pictorial)
                            <li>
                                <a href="{{ route('front.pictorials.show', ['pictorial' => $footer_pictorial]) }}">
                                    <img src="{{ $footer_pictorial->image ? asset($footer_pictorial->image) : asset('front/img/no-image.jpg') }}" alt="{{ $footer_pictorial->title }}">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="one_third">
                <h6 class="heading">{!! nl2br(option('info_address')) !!}</h6>
                <p class="nospace btmspace-15">{{ __('messages.footer.subscribe-to-newsletter') }}</p>
                <form id="newsletter-create-form" method="post" action="{{ route('front.newsletters.store') }}">
                    <fieldset>
                        <legend>{{ __('messages.footer.newsletter') }}</legend>
                        <input class="btmspace-15" type="text" name="name" placeholder="{{ __('messages.footer.form-name') }}" required>
                        <input class="btmspace-15" type="email" name="email" placeholder="{{ __('messages.footer.form-email') }}" required>
                        <input class="btmspace-15" autocomplete="off" type="text" name="captcha" placeholder="{{ __('messages.footer.form-captcha') }}" required>
                        <img class="captcha mt-0 mb-3" src="{{ captcha_src('flat') }}" alt="captcha">
                        <button type="submit" value="submit">{{ __('messages.footer.submit-newsletter') }}</button>
                    </fieldset>
                </form>
            </div>
        </footer>
    </div>
    <div class="wrapper row5">
        <div id="copyright" class="hoc clear">
            <p>{{ option('info_footer_text') }}</p>
            
        </div>
    </div>
    <a id="backtotop" href="#top"><i class="fas fa-chevron-up"></i></a>
</footer>