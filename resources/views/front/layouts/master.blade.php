<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <title>{{ option('info_site_title', 'ideal') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('front/css/layout.css') }}" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">
    
    <!-- plugins css files -->
    <link rel="stylesheet" href="{{ asset('front/js/plugins/toastr/toastr.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/custom.css') }}">
    
    <!-- RTL -->

    @if (local_info()['direction'] == 'rtl')
        <link rel="stylesheet" href="{{ asset('front/css/bootstrap-rtl.min.css') }}">
        <link rel="stylesheet" href="{{ asset('front/css/rtl.css') }}">
    @endif

    <!-- RTL -->
    
    @stack('styles')
</head>

<body>
    @include('front.layouts.header')

    @yield('content')


    @include('front.layouts.footer')
    <!-- JAVASCRIPTS -->
    <script src="{{ asset('front/js/jquery.min.js') }}"></script>
    <script src="{{ asset('front/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('front/js/plugins/jquery.blockUI.js') }}"></script>
    <script src="{{ asset('front/js/plugins/toastr/toastr.min.js') }}"></script>

    <script src="{{ asset('front/js/scripts.js') }}"></script>

    <script>
        var BASE_URL = "{{ route('front.index') }}";

    </script>

    <script src="{{ asset('front/js/pages/comments.js') }}"></script>

    @stack('scripts')
</body>

</html>
