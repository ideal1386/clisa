<div>
    <div class="head-tap"></div>
    <div class="wrapper row0">
        <header id="header" class="hoc">
            <div id="logo" class="one_quarter first">
                <h1><a href="{{ route('front.index') }}">{{ option('info_site_title', 'ideal') }}</a></h1>
            </div>
            <div class="lang-flags">
                <a href="{{ url('fa') }}">
                    <img class="img-fluid mx-1 rounded" src="{{ asset('./front/img/iran.jpg') }}" alt="fa">
                </a>
                <a href="{{ url('en') }}">
                    <img class="img-fluid mx-1 rounded" src="{{ asset('./front/img/english.jpg') }}" alt="en">
                </a>
            </div>
        </header>
        
    </div>
    <div class="row1">
        <section class="menu container">
            @include('front.partials.menu.menu')

        </section>
    </div>
</div>
