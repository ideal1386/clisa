@extends('front.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm 12 col-9">
           
                <div class="title">
                    <h3 class="my-3">
                        {{$page->title }}
                    </h3>
                </div>
                <div class="body text-justify px-3">
                    {!! $page->content !!}
                </div>

            </div>

        </div>
    </div>
@endsection