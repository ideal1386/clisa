@extends('front.layouts.master')

@section('content')
    <div class="container">
        <h3>{{ __('messages.page.blog') }}</h3>
        <div class="row">
            @foreach ($posts as $post)
                <div class="col-sm-6 col-md-4 col-lg-3 my-2">
                    <div class="postv">
                        <div class="post-img">
                            <img src="{{ $post->image ? asset($post->image) : asset('front/img/no-image.jpg') }}" alt="{{ $post->title }}">
                        </div>
                        <div class="post-title pt-3 pl-2">
                            <h5 class="text-info">{{ $post->title }}</h5>
                        </div>
                        <div class="post-sum">
                            <p>
                                {!! $post->shortContent(30) !!}
                            </p>
                            <a class="btn btn-warning" href="{{ route('front.posts.show', ['post' => $post]) }}">{{ __('messages.page.read-more') }}</a>
                        </div>
                    </div>
                </div>    
            @endforeach
        </div>
    </div>
@endsection
