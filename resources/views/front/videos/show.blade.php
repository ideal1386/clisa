@extends('front.layouts.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('front/css/video.css') }}">
@endpush

@section('content')
    <main class="container">
        <!-- main body -->
        <div class="content">
            <div class="row">
                <div class="col-md-9">
                    <div class="video-section">
                        @if ($video->video_type == 'locale-video')
                            <div class="text-center">
                                <video controlslist="nodownload" controls src="{{ $video->video }}" style="height: auto;" width="95%">Your browser does not support the video tag.</video>
                            </div>
                        @else
                            <div class="youtube-div">
                                {!! youtube_iframe($video->youtube) !!}
                            </div>
                        @endif
                    </div>
                    <h3 class="mt-3">{{ $video->title }}</h3>

                    <div>
                        {!! $video->content !!}
                    </div>

                    @include('front.components.comments', ['model' => $video, 'route_link' => route('front.video.comments', ['video' => $video]) ])

                </div>
                
                @include('front.partials.sidebar')
            </div>
        </div>
        </div>
        <!-- / main body -->
    </main>
@endsection
