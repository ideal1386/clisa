@extends('front.layouts.master')

@section('content')
    <div class="container">
        <h3>{{ $category->title }}</h3>
        <div class="row">
            @foreach ($videos as $video)
                <div class="col-sm-6 col-md-4 col-lg-3 my-2">
                    <div class="postv">
                        <div class="post-img">
                            <img src="{{ $video->image ? asset($video->image) : asset('front/img/no-image.jpg') }}" alt="{{ $video->title }}">
                        </div>
                        <div class="post-title pt-3 pl-2">
                            <h5 class="text-info">{{ $video->title }}</h5>
                        </div>
                        <div class="post-sum">
                            <p>
                                {!! $video->shortContent(30) !!}
                            </p>
                            <a class="btn btn-warning" href="{{ route('front.videos.show', ['video' => $video]) }}">{{ __('messages.videos.view-more') }}</a>
                        </div>
                    </div>
                </div>    
            @endforeach
        </div>
    </div>
@endsection
