@extends('front.layouts.master')

@section('content')
    <main class="container">
        <!-- main body -->
        <div class="row">
            <div class="col-md-6">
                <h2>{{ __('messages.contact.contact') }}</h2>
                <form id="contact-form" class="border p-4 mt-3 rounded"  action="{{ route('front.contact.store') }}">
                    <div class="form-group">
                        <label for="name">{{ __('messages.contact.name') }}</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('messages.contact.name-placeholder') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="subject">{{ __('messages.contact.subject') }}</label>
                        <input type="text" name="subject" class="form-control" id="subject" placeholder="" required>
                    </div>
                    <div class="form-group">
                        <label for="email">{{ __('messages.contact.email-address') }}</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="name@example.com" required>
                    </div>
                    <div class="form-group">
                        <label for="message">{{ __('messages.contact.message') }}</label>
                        <textarea class="form-control" name="message" id="message" rows="3" required></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="captcha" autocomplete="off" class="form-control" id="captcha" placeholder="{{ __('messages.contact.captcha') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img class="captcha" src="{{ captcha_src('flat') }}" alt="captcha">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">{{ __('messages.contact.send') }}</button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <h2>{{ __('messages.contact.our-location') }}</h2>
                <div class="map">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="contactinfo d-flex mt-5">
                @if (option('info_tel'))
                    <div class="d-flex flex-row">
                        <span class=" mr-3 fas fa-phone"></span>
                        <p>{{ option('info_tel') }}</p>
                    </div>
                @endif

                @if (option('info_fax'))
                    <div class="d-flex flex-row">
                        <span class=" mr-3 fas fa-fax"></span>
                        <p>{{ option('info_fax') }}</p>
                    </div>
                @endif

                @if (option('info_email'))
                    <div class="d-flex flex-row">
                        <span class=" mr-3 fas fa-envelope"></span>
                        <p>{{ option('info_email') }}</p>
                    </div>
                @endif

                @if (option('info_address'))
                    <div class="d-flex flex-row">
                        <span class=" mr-3 fas fa-map-marker-alt"></span>
                        <p>{{ option('info_address') }}</p>
                    </div>
                @endif

            </div>
        </div>
        <!-- / main body -->
    </main>
@endsection

@push('scripts')
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdbCAXvJIl7CKZwfpTswAIHvqJmZTUPwQ"></script>

    <script>
        if (typeof google !== 'undefined') {
            var text = "{{ option('info_site_title', 'ایده آل آی تی') }}";
            var myLatlng = new google.maps.LatLng({{ option('info_latitude', '38.07709880960678') }}, {{ option('info_Longitude', '46.28582686185837') }});
        }
    </script>

    <script src="{{ asset('front/js/pages/contact.js') }}"></script>
@endpush