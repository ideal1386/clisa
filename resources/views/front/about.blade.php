@extends('front.layouts.master')

@section('content')
<div class="wrapper row3">
    <main class="hoc container clear">
        <div class="row">
            <div class="col">
                <h1>
                    About Us
                </h1>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus dolorem, reprehenderit laboriosam, reiciendis saepe aliquid sapiente, in repellendus a libero magni quam quis quisquam consectetur blanditiis accusantium maiores voluptates tempora.
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus dolorem, reprehenderit laboriosam, reiciendis saepe aliquid sapiente, in repellendus a libero magni quam quis quisquam consectetur blanditiis accusantium maiores voluptates tempora.
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus dolorem, reprehenderit laboriosam, reiciendis saepe aliquid sapiente, in repellendus a libero magni quam quis quisquam consectetur blanditiis accusantium maiores voluptates tempora.
                </p>
            </div>
            <div class="col">
                <img src="/front/img/15647966_401.jpg" alt="">
            </div>
        </div>
    </main>
</div>
@endsection