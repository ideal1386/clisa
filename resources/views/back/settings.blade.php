@extends('back.layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="/back/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/plugins/jquery-tagsinput/jquery.tagsinput.min.css">
@endpush

@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- users edit start -->
            <section class="users-edit">
                <div class="card">
                    <div id="main-card" class="card-content">
                        <div class="card-body">
                            <ul class="nav nav-tabs mb-3" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center active" id="profile-tab" data-toggle="tab" href="#profile" aria-controls="profile" role="tab" aria-selected="true">
                                        <i class="feather icon-user mr-25"></i><span class="d-none d-sm-block">پروفایل</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center" id="information-tab" data-toggle="tab" href="#information" aria-controls="information" role="tab" aria-selected="false">
                                        <i class="feather icon-info mr-25"></i><span class="d-none d-sm-block">اطلاعات سایت</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex align-items-center" id="social-tab" data-toggle="tab" href="#social" aria-controls="social" role="tab" aria-selected="false">
                                        <i class="feather icon-share-2 mr-25"></i><span class="d-none d-sm-block">شبکه های اجتماعی</span>
                                    </a>
                                </li>


                                @if(auth()->user()->level == 'creator')
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" id="sizes-tab" data-toggle="tab" href="#sizes" aria-controls="sizes" role="tab" aria-selected="false">
                                            <i class="feather icon-image mr-25"></i><span class="d-none d-sm-block">اندازه تصاویر</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                            <div class="tab-content">
                                @include('back.partials.settings.profile-tab')

                                @include('back.partials.settings.information-tab')

                                @include('back.partials.settings.social-tab')

                                @include('back.partials.settings.sizes-tab')
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- users edit ends -->

        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="/back/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/back/app-assets/plugins/jquery-tagsinput/jquery.tagsinput.min.js"></script>
    <script src="/back/app-assets/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="/back/app-assets/plugins/jquery-validation/localization/messages_fa.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdbCAXvJIl7CKZwfpTswAIHvqJmZTUPwQ"></script>

    <script>
        
        if (typeof google !== 'undefined') {
            var myLatlng = new google.maps.LatLng({{ option('info_latitude', '38.07709880960678') }}, {{ option('info_Longitude', '46.28582686185837') }});
        }

    </script>

    <script src="/back/assets/js/pages/settings.js"></script>
    <script src="/back/app-assets/js/scripts/navs/navs.js"></script>

@endpush