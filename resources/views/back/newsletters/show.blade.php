<div class="table-responsive">
    <table class="table">
        <tbody>
            
            <tr>
                <th scope="row">نام</th>
                <td>{{ $newsletter->name }}</td>
                
            </tr>

            <tr>
                <th scope="row">موضوع</th>
                <td>{{ $newsletter->subject }}</td>
            </tr>

            <tr>
                <th scope="row">تاریخ ارسال</th>
                <td>{{ verta($newsletter->created_at) }}</td>
            </tr>

            <tr>
                <th scope="row">پیام</th>
                <td>{{ $newsletter->message }}</td>
            </tr>

        </tbody>
    </table>
</div>