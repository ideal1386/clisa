<table>
    <thead>
    <tr>
        <th>شناسه</th>
        <th>نام</th>
        <th>ایمیل</th>
        <th>تاریخ ثبت نام</th>

    </tr>
    </thead>
    <tbody>
    @foreach($newsletters as $newsletter)
        <tr>
            <td>{{ $newsletter->id }}</td>
            <td>{{ $newsletter->name }}</td>
            <td>{{ $newsletter->email }}</td>
            <td>{{ verta($newsletter->created_at) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>