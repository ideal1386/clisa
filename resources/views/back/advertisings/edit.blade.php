@extends('back.layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="/back/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/plugins/jquery-tagsinput/jquery.tagsinput.min.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/plugins/jquery-ui/jquery-ui.css">
@endpush

@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item">مدیریت
                                </li>
                                <li class="breadcrumb-item">مدیریت آگهی ها
                                </li>
                                <li class="breadcrumb-item active">ویرایش آگهی
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="content-body">
            <!-- Description -->
            <section id="description" class="card">
                <div class="card-header">
                    <h4 class="card-title">ویرایش آگهی </h4>
                </div>
                
                <div id="main-card" class="card-content">
                    <div class="card-body">
                        <div class="col-12 col-md-10 offset-md-1">
                            <form class="form" id="advertising-edit-form" action="{{ route('admin.advertisings.update', ['advertising' => $advertising]) }}" data-redirect="{{ route('admin.advertisings.index') }}" method="advertising">
                                @csrf
                                @method('put')
                                <div class="form-body">
                                    <div class="row">
                                        
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>دسته بندی</label>
                                                <select class="form-control" id="category" name="category_id">
                                                    @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}" @if($advertising->category->id == $category->id) selected @endif>{{ $category->fullTitle }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label>تصویر شاخص</label>
                                                <div class="custom-file">
                                                    <input id="image" type="file" accept="image/*" name="image" class="custom-file-input">
                                                    <label class="custom-file-label" for="image">{{ $advertising->image }}</label>
                                                    <p><small>بهترین اندازه <span class="text-danger">{{ option('sizes_advertising_image') }}</span> پیکسل میباشد.</small></p>

                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-md-6 mb-2">
                                            <fieldset class="checkbox">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                    <input type="checkbox" name="published" {{ $advertising->published ? 'checked' : '' }}>
                                                    <span class="vs-checkbox">
                                                        <span class="vs-checkbox--check">
                                                            <i class="vs-icon feather icon-check"></i>
                                                        </span>
                                                    </span>
                                                    <span>انتشار آگهی؟</span>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ویرایش آگهی</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Description -->
            
        </div>
    </div>
</div>

@endsection

@push('scripts') 
    <script src="/back/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/back/app-assets/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="/back/app-assets/plugins/jquery-validation/localization/messages_fa.min.js"></script>

    <script src="/back/assets/js/pages/advertisings/edit.js"></script>
@endpush