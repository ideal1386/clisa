@extends('back.layouts.master')

@section('content')

    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb no-border">
                                    <li class="breadcrumb-item">مدیریت
                                    </li>
                                    <li class="breadcrumb-item">مدیریت کاربران
                                    </li>
                                    <li class="breadcrumb-item active">لیست کاربران
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if($users->count())
                    <section id="description" class="card">
                        <div class="card-header">
                            <h4 class="card-title">لیست کاربران</h4>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a class="btn btn-outline-primary waves-effect waves-light excel-export" href="{{ route('admin.users.export') }}"><i class="fa fa-file-excel-o"></i> خروجی اکسل</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content" id="main-card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th class="text-center">آیدی</th>
                                                <th>نام کاربری</th>
                                                <th>ایمیل</th>
                                                <th class="text-center">عملیات</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <tr id="user-{{ $user->id }}-tr">
                                                    <th class="text-center">{{ $user->id }}</th>

                                                    <td>{{  $user->username }}</td>
                                                    <td>{{ $user->email ?: '--' }}</td>
                                                   
                                                    <td class="text-center">
                                                        <a href="{{ route('admin.users.show', ['user' => $user]) }}" class="btn btn-success mr-1 waves-effect waves-light">مشاهده</a>
                                                        @if (true || $user->id != auth()->user()->id)
                                                            <a href="{{ route('admin.users.edit', ['user' => $user]) }}" class="btn btn-warning mr-1 waves-effect waves-light">ویرایش</a>
                                                        @else
                                                            <button type="button" class="btn btn-warning mr-1 waves-effect waves-light" disabled>ویرایش</button>

                                                        @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                @else
                    <section class="card">
                        <div class="card-header">
                            <h4 class="card-title">لیست کاربران</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="card-text">
                                    <p>چیزی برای نمایش وجود ندارد!</p>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif

                {{ $users->links() }}

            </div>
        </div>
    </div>


@endsection