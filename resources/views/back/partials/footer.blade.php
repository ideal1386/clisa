<footer class="footer footer-static footer-light">
    <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">کپی رایت &copy; {{ date('Y') }}<a class="text-bold-800 grey darken-2" href="https://ideal-it.ir" target="_blank">ایده آل آی تی,</a>تمامی حقوق محفوظ است</span><span class="float-md-right d-none d-md-block">طراحی و توسعه با <i class="feather icon-heart pink"></i>  توسط ایده آل آی تی</span>
    </p>
</footer>