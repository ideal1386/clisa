<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{ route('front.index') }}" target="_blank">
                    <h2 class="brand-text mb-0">ایده آل</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                        class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                        class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary"
                        data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="{{ active_class('admin.dashboard') }} nav-item"><a href="{{ route('admin.dashboard') }}">
                    <i class="feather icon-home"></i>
                    <span class="menu-title">داشبورد</span>
                </a>
            </li>
            <li class="nav-item has-sub {{ open_class(['admin.users.show']) }}"><a href="#"><i class="feather icon-users"></i><span class="menu-title" >مدیریت کاربران</span></a>
                <ul class="menu-content">
                    <li class="{{ active_class('admin.users.index') }}"><a href="{{ route('admin.users.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست کاربران</span></a>
                    </li>
                    <li class="{{ active_class('admin.users.create') }}"><a href="{{ route('admin.users.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد کاربر</span></a>
                    </li>

                </ul>
            </li>
            <li class="nav-item has-sub {{ open_class(['admin.posts.edit']) }}"><a href="#"><i class="feather icon-file-text"></i><span class="menu-title" >مدیریت وبلاگ</span></a>
                <ul class="menu-content">
                    <li class="{{ active_class('admin.posts.index') }}"><a href="{{ route('admin.posts.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست نوشته ها</span></a>
                    </li>
                    <li class="{{ active_class('admin.posts.create') }}"><a href="{{ route('admin.posts.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد نوشته</span></a>
                    </li>
                    <li class="{{ active_class('admin.posts.categories.index') }}"><a href="{{ route('admin.posts.categories.index') }}"><i class="feather icon-circle"></i><span class="menu-item">دسته بندی ها</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-sub {{ open_class(['admin.videos.edit']) }}"><a href="#"><i class="feather icon-file-text"></i><span class="menu-title" >مدیریت ویدیوها</span></a>
                <ul class="menu-content">
                    <li class="{{ active_class('admin.videos.index') }}"><a href="{{ route('admin.videos.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست ویدیو ها</span></a>
                    </li>
                    <li class="{{ active_class('admin.videos.create') }}"><a href="{{ route('admin.videos.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد ویدیو</span></a>
                    </li>
                    <li class="{{ active_class('admin.videos.categories.index') }}"><a href="{{ route('admin.videos.categories.index') }}"><i class="feather icon-circle"></i><span class="menu-item">دسته بندی ها</span></a>
                    </li>
                </ul>
            </li>
            
            
            <li class="nav-item has-sub {{ open_class(['admin.pictorials.edit']) }}"><a href="#"><i class="feather icon-image"></i><span class="menu-title" >مدیریت به روایت تصویر</span></a>
                <ul class="menu-content">
                    <li class="{{ active_class('admin.pictorials.index') }}"><a href="{{ route('admin.pictorials.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست به روایت تصویر</span></a>
                    </li>
                    <li class="{{ active_class('admin.pictorials.create') }}"><a href="{{ route('admin.pictorials.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد به روایت تصویر</span></a>
                    </li>
                    <li class="{{ active_class('admin.pictorials.categories.index') }}"><a href="{{ route('admin.pictorials.categories.index') }}"><i class="feather icon-circle"></i><span class="menu-item">دسته بندی ها</span></a>
                    </li>
                </ul>
            </li>

            <li class="nav-item has-sub {{ open_class(['admin.sliders.show', 'admin.sliders.edit']) }}"><a href="#"><i class="feather icon-sliders"></i><span class="menu-title" >مدیریت اسلایدرها</span></a>
                <ul class="menu-content">
                    <li class="{{ active_class('admin.sliders.index') }}"><a href="{{ route('admin.sliders.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست اسلایدرها</span></a>
                    </li>
                    <li class="{{ active_class('admin.sliders.create') }}"><a href="{{ route('admin.sliders.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد اسلایدر</span></a>
                    </li>
                </ul>
            </li>
            {{-- <li class="nav-item has-sub {{ open_class(['admin.banners.show', 'admin.banners.edit']) }}"><a href="#"><i class="feather icon-image"></i><span class="menu-title" >مدیریت بنرها</span></a>
                <ul class="menu-content">
                    <li class="{{ active_class('admin.banners.index') }}"><a href="{{ route('admin.banners.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست بنرها</span></a>
                    </li>
                    <li class="{{ active_class('admin.banners.create') }}"><a href="{{ route('admin.banners.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد بنر</span></a>
                    </li>
                </ul>
            </li> --}}

            {{-- <li class="nav-item has-sub {{ open_class(['admin.links.show', 'admin.links.edit']) }}"><a href="#"><i class="feather icon-link"></i><span class="menu-title" >مدیریت لینک های فوتر</span></a>
                <ul class="menu-content">
                    <li class="{{ active_class('admin.links.index') }}"><a href="{{ route('admin.links.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست لینک ها</span></a>
                    </li>
                    <li class="{{ active_class('admin.links.create') }}"><a href="{{ route('admin.links.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد لینک </span></a>
                    </li>
                    <li class="{{ active_class('admin.links.groups.index') }}"><a href="{{ route('admin.links.groups.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست گروه ها </span></a>
                    </li>
                </ul>
            </li> --}}

            <li class="nav-item has-sub {{ open_class(['admin.pages.edit']) }}"><a href="#"><i class="feather icon-file"></i><span class="menu-title" >مدیریت صفحات</span></a>
                <ul class="menu-content">
                    <li class="{{ active_class('admin.pages.index') }}"><a href="{{ route('admin.pages.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست صفحات</span></a>
                    </li>
                    <li class="{{ active_class('admin.pages.create') }}"><a href="{{ route('admin.pages.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد صفحه</span></a>
                    </li>
                    
                </ul>
            </li>

            {{-- <li class="{{ active_class('admin.widgets.index') }} nav-item"><a href="{{ route('admin.widgets.index') }}">
                    <i class="feather icon-layout"></i>
                    <span class="menu-title">مدیریت ابزارک ها</span>
                </a>
            </li> --}}
        
            <li class="{{ active_class('admin.file-manager') }} nav-item"><a href="{{ route('admin.file-manager') }}">
                    <i class="feather icon-folder"></i>
                    <span class="menu-title">مدیریت فایل ها</span>
                </a>
            </li>

            <li class="{{ active_class('admin.menus.index') }} nav-item"><a href="{{ route('admin.menus.index') }}">
                    <i class="feather icon-menu"></i>
                    <span class="menu-title">مدیریت منوها</span>
                </a>
            </li>

            <li class="{{ active_class('admin.contacts.index') }} nav-item"><a href="{{ route('admin.contacts.index') }}">
                    <i class="feather icon-message-square"></i>
                    <span class="menu-title">لیست تماس با ما</span>
                </a>
            </li>
            <li class="{{ active_class('admin.newsletters.index') }} nav-item"><a href="{{ route('admin.newsletters.index') }}">
                    <i class="feather icon-mail"></i>
                    <span class="menu-title">لیست خبرنامه</span>
                </a>
            </li>
            <li class="{{ active_class('admin.comments.index') }} nav-item"><a href="{{ route('admin.comments.index') }}">
                    <i class="feather icon-message-circle"></i>
                    <span class="menu-title">مدیریت نظرات</span>
                </a>
            </li>
            <li class="{{ active_class('admin.notifications') }} nav-item"><a href="{{ route('admin.notifications') }}">
                    <i class="feather icon-bell"></i>
                    <span class="menu-title">اعلان ها</span>
                    @if($notifications->count())
                        <span class="badge badge badge-primary badge-pill float-right mr-2"> {{ $notifications->count() }}</span>
                    @endif
                </a>
            </li>
            <li class="{{ active_class('admin.settings') }} nav-item">
                <a href="{{ route('admin.settings') }}">
                    <i class="feather icon-settings"></i>
                    <span class="menu-title">تنظیمات</span>
                </a>
            </li>

        </ul>
    </div>
</div>
