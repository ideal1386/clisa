<div class="tab-pane" id="social" aria-labelledby="social-tab" role="tabpanel">
    <!-- users edit socail form start -->
    <form id="socials-form" action="{{ route('admin.settings.socials') }}" method="POST">
        <div class="row">
            <div class="col-12 col-sm-6">

                <fieldset>
                    <label>تلگرام</label>
                    <div class="input-group mb-75">
                        
                        <input type="text" name="social_telegram" class="form-control ltr" value="{{ option('social_telegram') }}">
                        <div class="input-group-append">
                            <span class="input-group-text fa fa-telegram"></span>
                        </div>
                    </div>

                    <label>اینستاگرام</label>
                    <div class="input-group mb-75">
                        
                        <input type="text" name="social_instagram" class="form-control ltr" value="{{ option('social_instagram') }}">
                        <div class="input-group-append">
                            <span class="input-group-text feather icon-instagram"></span>
                        </div>
                    </div>

                    <label>واتساپ</label>
                    <div class="input-group mb-75">
                        
                        <input type="text" name="social_whatsapp" class="form-control ltr" value="{{ option('social_whatsapp') }}">
                        <div class="input-group-append">
                            <span class="input-group-text fa fa-whatsapp"></span>
                        </div>
                    </div>

                    <label>توییتر</label>
                    <div class="input-group mb-75">
                        <input type="text" name="social_twitter" class="form-control ltr" value="{{ option('social_twitter') }}">
                        <div class="input-group-append">
                            <span class="input-group-text feather icon-twitter"></span>
                        </div>
                    </div>
                    
                </fieldset>
            </div>
            <div class="col-12 col-sm-6">

                
                <label>فیسبوک</label>
                <div class="input-group mb-75">
                    <input type="text" name="social_facebook" class="form-control ltr" value="{{ option('social_facebook') }}">
                    <div class="input-group-append">
                        <span class="input-group-text feather icon-facebook"></span>
                    </div>
                </div>

                <label>یوتیوب</label>
                <div class="input-group mb-75">
                    <input type="text" name="social_youtube" class="form-control ltr" value="{{ option('social_youtube') }}">
                    <div class="input-group-append">
                        <span class="input-group-text fa fa-youtube"></span>
                    </div>
                </div>

                <label>لینکدین</label>
                <div class="input-group mb-75">
                    <input type="text" name="social_linkedin" class="form-control ltr" value="{{ option('social_linkedin') }}">
                    <div class="input-group-append">
                        <span class="input-group-text fa fa-linkedin"></span>
                    </div>
                </div>
            </div>
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">ذخیره تغییرات</button>
                
            </div>
        </div>
    </form>
    <!-- users edit socail form ends -->
</div>