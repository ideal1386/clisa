<div class="tab-pane" id="information" aria-labelledby="information-tab" role="tabpanel">
    <!-- users edit Info form start -->
    <form id="information-form" action="{{ route('admin.settings.information') }}" method="POST">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="controls">
                        <label>عنوان وبسایت</label>
                        <input type="text" name="info_site_title" class="form-control" value="{{ option('info_site_title') }}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="basicInputFile">آیکون</label>
                    <div class="custom-file">
                        <input type="file" accept="image/*" name="info_icon" class="custom-file-input">
                        <label class="custom-file-label" for="inputGroupFile01">{{ option('info_icon') }}</label>
                        <p><small>بهترین اندازه <span class="text-danger">{{ option('sizes_icon') }}</span> پیکسل میباشد.</small></p>

                    </div>
                </fieldset>
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="basicInputFile">لوگوی شرکت</label>
                    <div class="custom-file">
                        <input type="file" accept="image/*" name="info_logo" class="custom-file-input">
                        <label class="custom-file-label" for="inputGroupFile01">{{ option('info_logo') }}</label>
                        <p><small>بهترین اندازه <span class="text-danger">{{ option('sizes_logo') }}</span> پیکسل میباشد.</small></p>
                    </div>
                </fieldset>
                
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="controls">
                        <label>تلفن</label>
                        <input type="text" name="info_tel" class="form-control" value="{{ option('info_tel') }}" >
                    </div>
                </div>
                
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="controls">
                        <label>فکس</label>
                        <input type="text" name="info_fax" class="form-control" value="{{ option('info_fax') }}">
                    </div>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="controls">
                        <label>کد پستی</label>
                        <input type="text" name="info_postal_code" class="form-control" value="{{ option('info_postal_code') }}">
                    </div>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label>کلمات کلیدی</label>
                    <input id="tags" type="text" name="info_tags" class="form-control" value="{{ option('info_tags') }}">
                </fieldset>
                
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label>توضیحات کوتاه</label>
                    <textarea name="info_short_description" class="form-control" rows="3">{{ option('info_short_description') }}</textarea>
                </fieldset>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <fieldset class="form-group">
                    <label>آدرس</label>
                    <textarea  name="info_address" class="form-control" rows="3">{{ option('info_address') }}</textarea>
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label>متن فوتر</label>
                    <textarea  name="info_footer_text" class="form-control" rows="3">{{ option('info_footer_text') }}</textarea>
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label>اسکریپت های اضافه</label>
                    <textarea  name="info_scripts" class="form-control ltr" rows="3">{{ option('info_scripts') }}</textarea>
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label>متن وبلاگ در صفحه اصلی</label>
                    <textarea  name="index_blog_text" class="form-control" rows="3">{{ option('index_blog_text') }}</textarea>
                </fieldset>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="controls">
                        <label>ایمیل</label>
                        <input type="text" name="info_email" class="form-control" value="{{ option('info_email') }}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="controls">
                        <label>شماره موبایل</label>
                        <input type="text" name="info_mobile" class="form-control" value="{{ option('info_mobile') }}">
                    </div>
                </div>
            </div>
        </div>

        <dir class="row p-0">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="controls">
                        <label>طول جغرافیایی</label>
                        <input type="text" id="Longitude" name="info_Longitude" class="form-control" value="{{ option('info_Longitude', '46.28582686185837') }}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="controls">
                        <label>عرض جغرافیایی</label>
                        <input type="text" id="latitude" name="info_latitude" class="form-control" value="{{ option('info_latitude', '38.07709880960678') }}">
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div id="map"></div>
            </div>

            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">ذخیره تغییرات</button>
                
            </div>
        </dir>
    </form>
    <!-- users edit Info form ends -->
</div>