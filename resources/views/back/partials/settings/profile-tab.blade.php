<div class="tab-pane active" id="profile" aria-labelledby="profile-tab" role="tabpanel">

    <!-- users edit media object start -->
    <div class="media mb-2">
        <a class="mr-2 my-25" href="#">
            <img id="profile-pic" src="{{ auth()->user()->imageUrl }}" alt="users avatar" class="users-avatar-shadow rounded" height="90" width="90">
        </a>
        <div class="media-body mt-50">
            
            <div class="col-12 d-flex mt-1 px-0">
                <button id="edit-image-btn" class="btn btn-primary mr-75">ویرایش تصویر</button>
            </div>
        </div>
    </div>
    <!-- users edit media object ends -->
    <!-- users edit profile form start -->
    <form id="profile-form" action="{{ route('admin.settings.profile') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
        <div class="row">
            <input type="file" name="image" id="profile-image" accept="image/*" style="display: none;">
            <div class="col-12 col-sm-6">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="controls">
                            <label>نام</label>
                            <input name="first_name" type="text" class="form-control" placeholder="نام" value="{{ auth()->user()->first_name }}" required >
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="controls">
                            <label>نام خانوادگی</label>
                            <input name="last_name" type="text" class="form-control" placeholder="نام خانوادگی" value="{{ auth()->user()->last_name }}" required >
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <div class="controls">
                        <label>نام کاربری</label>
                        <input name="username" type="text" class="form-control" placeholder="نام کاربری" value="{{ auth()->user()->username }}" required >
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <div class="controls">
                        <label>گذرواژه</label>
                        <input id="password" name="password" type="text" class="form-control pw" placeholder="گذرواژه">
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <label>تکرار گذرواژه</label>
                        <input name="password_confirmation" type="text" class="form-control pw" placeholder="تکرار گذرواژه">
                    </div>
                </div>
                
            </div>
            <div class="col-12">
                <div class="alert alert-info mt-1 alert-validation-msg" role="alert">
                    <i class="feather icon-info ml-1 align-middle"></i>
                    <span>در صورتی که نمیخواهید گذرواژه تان را عوض کنید، فیلدهای گذرواژه را خالی بگذارید.</span>
                </div>
            </div>
            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">ذخیره تغییرات</button>
            </div>
        </div>
    </form>
    <!-- users edit profile form ends -->
</div>