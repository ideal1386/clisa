@if (auth()->user()->level == 'creator')
    <div class="tab-pane" id="sizes" aria-labelledby="sizes-tab" role="tabpanel">
        <!-- users edit socail form start -->
        <form id="sizes-form" action="{{ route('admin.settings.sizes') }}" method="POST">
            <div class="row">
                <div class="col-md-6">
                    <label>تصویر شاخص محصولات</label>
                    <div class="input-group mb-75">
                        <input type="text" name="sizes_product_image" class="form-control ltr"
                            value="{{ option('sizes_product_image') }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <label>تصاویر گالری محصول</label>
                    <div class="input-group mb-75">
                        <input type="text" name="sizes_product_gallery" class="form-control ltr"
                            value="{{ option('sizes_product_gallery') }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <label>لوگو</label>
                    <div class="input-group mb-75">
                        <input type="text" name="sizes_logo" class="form-control ltr"
                            value="{{ option('sizes_logo') }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <label>آیکون</label>
                    <div class="input-group mb-75">
                        <input type="text" name="sizes_icon" class="form-control ltr"
                            value="{{ option('sizes_icon') }}">
                    </div>
                </div>

                <div class="col-md-6">
                    <label>تصویر شاخص پست ها</label>
                    <div class="input-group mb-75">
                        <input type="text" name="sizes_post_image" class="form-control ltr"
                            value="{{ option('sizes_post_image') }}">
                    </div>
                </div>

                <div class="col-md-6">
                    <label>تصویر شاخص آگهی ها</label>
                    <div class="input-group mb-75">
                        <input type="text" name="sizes_advertising_image" class="form-control ltr"
                            value="{{ option('sizes_advertising_image') }}">
                    </div>
                </div>

                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">ذخیره تغییرات</button>

                </div>
            </div>
        </form>
        <!-- users edit socail form ends -->
    </div>
@endif
