@extends('back.layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="/back/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/vendors/css/file-uploaders/dropzone.min.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/plugins/jquery-tagsinput/jquery.tagsinput.min.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/plugins/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/plugins/file-uploaders/dropzone.css">
@endpush

@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item">مدیریت
                                </li>
                                <li class="breadcrumb-item">مدیریت به روایت تصویر
                                </li>
                                <li class="breadcrumb-item active">ویرایش به روایت تصویر
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="content-body">
            <!-- Description -->
            <section class="card">
                <div class="card-header">
                    <h4 class="card-title">ویرایش به روایت تصویر</h4>
                </div>
                
                <div id="main-card" class="card-content">
                    <div class="card-body">
                        <div class="col-12 col-md-10 offset-md-1">
                            <form class="form" id="pictorial-edit-form" action="{{ route('admin.pictorials.update', ['pictorial' => $pictorial]) }}" method="post">
                                @csrf
                                @method('put')
                                <div class="form-body">
                                  
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>عنوان</label>
                                                <input type="text" class="form-control" name="title" value="{{ $pictorial->title }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>دسته بندی</label>
                                                <select class="form-control" id="category" name="category_id">
                                                    @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}" @if($pictorial->category->id == $category->id) selected @endif>{{ $category->fullTitle }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p><small>بهترین اندازه <span class="text-danger">450 * 750</span> پیکسل میباشد.</small></p>

                                            <div class="dropzone dropzone-area mb-2" id="pictorial-images">
                                                <div class="dz-message">تصاویر را به اینجا بکشید</div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="first-name-vertical">توضیحات</label>
                                                <textarea id="content" class="form-control" rows="3" name="content">{{ $pictorial->content }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <fieldset class="form-group">
                                                <label>کلمات کلیدی</label>
                                                <input type="text" name="tags" class="form-control tags" value="{{ $pictorial->getTags }}">
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <fieldset class="form-group">
                                                <label>تصویر شاخص</label>
                                                <div class="custom-file">
                                                    <input id="image" type="file" accept="image/*" name="image" class="custom-file-input">
                                                    <label class="custom-file-label" for="image">{{ $pictorial->image }}</label>
                                                    <p><small>بهترین اندازه <span class="text-danger">500 * 500</span> پیکسل میباشد.</small></p>

                                                </div>
                                            </fieldset>

                                            <fieldset class="checkbox">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                    <input type="checkbox" name="published" {{ $pictorial->published ? 'checked' : '' }}>
                                                    <span class="vs-checkbox">
                                                        <span class="vs-checkbox--check">
                                                            <i class="vs-icon feather icon-check"></i>
                                                        </span>
                                                    </span>
                                                    <span>انتشار به روایت تصویر؟</span>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ویرایش به روایت تصویر</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Description -->
            <div id="form-progress" class="progress progress-bar-success progress-xl" style="display: none;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width:0%">0%</div>
            </div>
            
        </div>
    </div>
</div>

@endsection

@push('scripts') 
        <script src="/back/app-assets/plugins/ckeditor/ckeditor.js"></script>
        <script src="/back/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/back/app-assets/vendors/js/extensions/dropzone.min.js"></script>

        <script src="/back/app-assets/plugins/jquery-tagsinput/jquery.tagsinput.min.js"></script>
        <script src="/back/app-assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/back/app-assets/plugins/jquery-ui/jquery-ui.js"></script>

        <script>
            /* load saved image gallery */
            var mockImages = [];
            @foreach($pictorial->gallery as $image)
                mockImages.push({ 
                    name: '{{ $image->image}}',
                    galleryImage: true,
                    type: 'image/jpeg',
                    status: 'success',
                    upload: {
                        filename: '{{ $image->image }}',
                    },
                    accepted: true,
                    image: '{{ $image->image }}',
                });
            @endforeach

            var pictorial = {{ $pictorial->id }};
        </script>

        <script src="/back/assets/js/pages/pictorials/all.js?v=1"></script>
        <script src="/back/assets/js/pages/pictorials/edit.js?v=1"></script>
@endpush
