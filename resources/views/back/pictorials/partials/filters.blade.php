<div class="card">
    <div class="card-header filter-card">
        <h4 class="card-title">فیلتر کردن</h4>
        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-content collapse {{ request()->all() ? 'show' : '' }}">
        <div class="card-body">
            <div class="users-list-filter">
                <form id="filter-comments-form" method="GET"
                      action="{{ route('admin.pictorials.index') }}">
                    <div class="row">
                        <div class="col-md-3">
                            <label>عنوان</label>
                            <fieldset class="form-group">
                                <input class="form-control" name="title" value="{{ request('title') }}">
                            </fieldset>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>دسته بندی</label>
                                <select class="form-control pictorial-category" name="category_id[]"
                                        multiple>
                                    <option></option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ ( request()->input('category_id') && in_array($category->id, request()->input('category_id')) ) ? 'selected' : '' }}>{{ $category->fullTitle }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>مرتب سازی</label>
                            <fieldset class="form-group">
                                <select class="form-control" name="ordering">
                                    <option value="latest" {{ request('ordering') == 'latest' ? 'selected' : '' }}>
                                        جدیدترین
                                    </option>
                                    <option value="oldest" {{ request('ordering') == 'oldest' ? 'selected' : '' }}>
                                        قدیمی ترین
                                    </option>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-md-3">
                            <label>تعداد در صفحه</label>
                            <fieldset class="form-group">
                                <select class="form-control" name="paginate">
                                    <option value="10" {{ request('paginate') == '10' ? 'selected' : '' }}>
                                        10
                                    </option>
                                    <option value="20" {{ request('paginate') == '20' ? 'selected' : '' }}>
                                        20
                                    </option>
                                    <option value="50" {{ request('paginate') == '50' ? 'selected' : '' }}>
                                        50
                                    </option>
                                </select>
                            </fieldset>
                        </div>

                        
                        <div class="col-12 text-right">
                            <button type="submit"
                                    class="btn btn-outline-success square  mb-1 waves-effect waves-light">
                                فیلتر کردن
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>