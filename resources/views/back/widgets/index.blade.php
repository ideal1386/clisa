@extends('back.layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('back/app-assets/vendors/css/forms/select/select2.min.css') }}">
@endpush

@section('content')

    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb no-border">
                                    <li class="breadcrumb-item">مدیریت
                                    </li>
                                    <li class="breadcrumb-item active">مدیریت ابزارک ها
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">

                <div class="row">
                    <div class="col-md-6">
                        @php
                            $widget = get_widget('index-up-left-sidebar')
                        @endphp

                        <section class="card">
                            <div class="card-header">
                                <h4 class="card-title">صفحه اصلی سایدبار بالا سمت چپ</h4>
                            </div>
                            
                            <div  class="card-content">
                                <div class="card-body">
                                    <div class="col-12">
                                        <form class="form widget-form" action="{{ route('admin.widgets.store') }}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    
                                                    <input type="hidden" name="name" value="index-up-left-sidebar">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>عنوان</label>
                                                            <input type="text" class="form-control" name="title" value="{{ $widget->title }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>دسته بندی</label>
                                                            <select class="form-control category"  name="category_id" required>
                                                                @foreach ($postcats as $category)
                                                                    <option value="{{ $category->id }}" {{ get_option_value($widget->options, 'category_id') == $category->id ? 'selected' : '' }}>{{ $category->fullTitle }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
        
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ذخیره</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        @php
                            $widget = get_widget('index-down-right-sidebar')
                        @endphp

                        <section class="card">
                            <div class="card-header">
                                <h4 class="card-title">صفحه اصلی سایدبار پایین سمت راست</h4>
                            </div>
                            
                            <div  class="card-content">
                                <div class="card-body">
                                    <div class="col-12">
                                        <form class="form widget-form" action="{{ route('admin.widgets.store') }}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    
                                                    <input type="hidden" name="name" value="index-down-right-sidebar">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>عنوان</label>
                                                            <input type="text" class="form-control" name="title" value="{{ $widget->title }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>دسته بندی</label>
                                                            <select class="form-control category"  name="category_id" required>
                                                                @foreach ($postcats as $category)
                                                                    <option value="{{ $category->id }}" {{ get_option_value($widget->options, 'category_id') == $category->id ? 'selected' : '' }}>{{ $category->fullTitle }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
        
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ذخیره</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-md-6">
                        @php
                            $widget = get_widget('index-recent-events')
                        @endphp

                        <section class="card">
                            <div class="card-header">
                                <h4 class="card-title">صفحه اصلی رویدادهای  اخیر</h4>
                            </div>
                            
                            <div  class="card-content">
                                <div class="card-body">
                                    <div class="col-12">
                                        <form class="form widget-form" action="{{ route('admin.widgets.store') }}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    
                                                    <input type="hidden" name="name" value="index-recent-events">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>عنوان</label>
                                                            <input type="text" class="form-control" name="title" value="{{ $widget->title }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>دسته بندی</label>
                                                            <select class="form-control category"  name="category_id" required>
                                                                @foreach ($postcats as $category)
                                                                    <option value="{{ $category->id }}" {{ get_option_value($widget->options, 'category_id') == $category->id ? 'selected' : '' }}>{{ $category->fullTitle }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
        
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ذخیره</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-md-6">
                        @php
                            $widget = get_widget('index-down-left-sidebar')
                        @endphp

                        <section class="card">
                            <div class="card-header">
                                <h4 class="card-title">صفحه اصلی سایدبار پایین سمت چپ</h4>
                            </div>
                            
                            <div  class="card-content">
                                <div class="card-body">
                                    <div class="col-12">
                                        <form class="form widget-form" action="{{ route('admin.widgets.store') }}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    
                                                    <input type="hidden" name="name" value="index-down-left-sidebar">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>عنوان</label>
                                                            <input type="text" class="form-control" name="title" value="{{ $widget->title }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>دسته بندی</label>
                                                            <select class="form-control category"  name="category_id" required>
                                                                @foreach ($postcats as $category)
                                                                    <option value="{{ $category->id }}" {{ get_option_value($widget->options, 'category_id') == $category->id ? 'selected' : '' }}>{{ $category->fullTitle }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
        
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ذخیره</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                    <div class="col-md-6">
                        @php
                            $widget = get_widget('index-down-left-video')
                        @endphp

                        <section class="card">
                            <div class="card-header">
                                <h4 class="card-title">صفحه اصلی ویدیو پایین سمت چپ</h4>
                            </div>
                            
                            <div  class="card-content">
                                <div class="card-body">
                                    <div class="col-12">
                                        <form class="form widget-form" action="{{ route('admin.widgets.store') }}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    
                                                    <input type="hidden" name="name" value="index-down-left-video">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>عنوان</label>
                                                            <input type="text" class="form-control" name="title" value="{{ $widget->title }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>دسته بندی</label>
                                                            <select class="form-control category"  name="category_id" required>
                                                                @foreach ($videocats as $category)
                                                                    <option value="{{ $category->id }}" {{ get_option_value($widget->options, 'category_id') == $category->id ? 'selected' : '' }}>{{ $category->fullTitle }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
        
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ذخیره</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        @php
                            $widget = get_widget('index-down-right-video')
                        @endphp

                        <section class="card">
                            <div class="card-header">
                                <h4 class="card-title">صفحه اصلی ویدیو پایین سمت راست</h4>
                            </div>
                            
                            <div  class="card-content">
                                <div class="card-body">
                                    <div class="col-12">
                                        <form class="form widget-form" action="{{ route('admin.widgets.store') }}">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    
                                                    <input type="hidden" name="name" value="index-down-right-video">
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>عنوان</label>
                                                            <input type="text" class="form-control" name="title" value="{{ $widget->title }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>دسته بندی</label>
                                                            <select class="form-control category"  name="category_id" required>
                                                                @foreach ($videocats as $category)
                                                                    <option value="{{ $category->id }}" {{ get_option_value($widget->options, 'category_id') == $category->id ? 'selected' : '' }}>{{ $category->fullTitle }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
        
                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ذخیره</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                    
                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('back/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

    <script src="{{ asset('back/assets/js/pages/widgets/index.js') }}"></script>
@endpush