<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<!-- BEGIN: Head-->

<head>
    @include('back.partials.meta')
    <title>
        @isset($title)
            {{  $title }}
        @else
            {{ option('info_site_title', 'ایده آل آی تی') }}
        @endisset
    </title>

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/back/app-assets/vendors/css/vendors-rtl.min.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/vendors/css/ui/prism.min.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/vendors/css/extensions/toastr.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/colors.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/components.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/themes/semi-dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/plugins/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/plugins/animate/animate.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    @stack('styles')
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/back/app-assets/css-rtl/custom-rtl.css">
    <link rel="stylesheet" type="text/css" href="/back/assets/css/style-rtl.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

    <!-- BEGIN: Header-->
    @include('back.partials.header')
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    @include('back.partials.sidebar')
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    @yield('content')
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('back.partials.footer')
    <!-- END: Footer-->

    <audio id="notification-sound" class="d-none">
        <source src="/back/app-assets/sounds/notification.ogg" type="audio/ogg">
        Your browser does not support the audio element.
    </audio>


    <!-- BEGIN: Vendor JS-->
    <script src="/back/app-assets/vendors/js/vendors.min.js"></script>
    <script src="/back/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/back/app-assets/vendors/js/extensions/toastr.min.js"></script>
    <script src="/back/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/back/app-assets/vendors/js/ui/prism.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/back/app-assets/js/core/app-menu.js"></script>
    <script src="/back/app-assets/js/core/app.js"></script>
    <script src="/back/assets/js/scripts.js"></script>
    <!-- END: Theme JS-->

    <script>
        var BASE_URL = "{{ route('admin.dashboard') }}";
    </script>

    <!-- BEGIN: Page JS-->
    @stack('scripts')
   
    @toastr_render
    <!-- END: Page JS-->

    <script src="/js/app.js"></script>
    <script>
        $(document).ready(function() {
            Echo.channel('orders').listen('OrderPaid', (event) => {

                toastr.info('سفارش شماره '  + event.order.id + ' با موفقیت ثبت و پرداخت شد', 'سفارش جدید ثبت شد', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left', timeOut: 0 });
                var sound = document.getElementById("notification-sound"); 
                sound.play(); 
            });
        });

    </script>

</body>
<!-- END: Body-->

</html>