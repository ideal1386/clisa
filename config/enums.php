<?php

return [
    'banners' => [
        'sidebar-banner' => [
            'name'  => 'تبلیغات  سایدبار',
            'group' => 'sidebar-banner'
        ],
    ],

    'static_menus' => [
        'posts' => [
            'title' => 'وبلاگ'
        ],
        'pictorials' => [
            'title' => 'به روایت تصویر'
        ],
        'videos' => [
            'title' => 'ویدیو ها'
        ]
    ]
];
