<?php

return [

    //-------------------------------
    // Timezone for insert dates in database
    // If you want Gateway not set timezone, just leave it empty
    //--------------------------------
    'timezone' => 'Asia/Tehran',

    // Gateways Config is set in AppServiceProvider file

    //------------ Tables names
    'table'    => 'gateway_transactions',
];
