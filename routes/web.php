<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Auth'], function () {
    // ------------------ Authentication Routes...
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');

    Route::get('logout', 'LoginController@logout');
});

Route::get('province/get-cities', 'ProvinceController@getCities');

Route::get('login', 'Back\MainController@login')->name('login');

// ------------------ Admin Part Routes
Route::group(['namespace' => 'Back', 'as' => 'admin.', 'prefix' => 'admin', 'middleware' => ['auth', 'Admin']], function () {

    // ------------------ MainController
    Route::get('/', 'MainController@index')->name('dashboard');
    Route::get('settings', 'MainController@settings')->name('settings');
    Route::post('settings/profile', 'MainController@profile')->name('settings.profile');
    Route::post('settings/socials', 'MainController@socials')->name('settings.socials');
    Route::post('settings/sizes', 'MainController@sizes')->name('settings.sizes');
    Route::post('settings/information', 'MainController@information')->name('settings.information');
    Route::get('get-tags', 'MainController@get_tags');

    Route::get('notifications', 'MainController@notifications')->name('notifications');

    Route::get('file-manager', 'MainController@fileManager')->name('file-manager');
    Route::get('file-manager-iframe', 'MainController@fileManagerIframe')->name('file-manager-iframe');

    // ------------------ users
    Route::resource('users', 'UserController');
    Route::get('users/export/excel', 'UserController@export')->name('users.export');

    // ------------------ widgets
    Route::resource('widgets', 'WidgetController');

    // ------------------ pictorials
    Route::resource('pictorials', 'PictorialController')->except('show');
    Route::post('pictorials/image-store', 'PictorialController@image_store');
    Route::post('pictorials/image-delete', 'PictorialController@image_delete');
    Route::get('pictorial/categories', 'PictorialController@categories')->name('pictorials.categories.index');


    // ------------------ posts
    Route::resource('posts', 'PostController')->except(['show']);
    Route::get('post/categories', 'PostController@categories')->name('posts.categories.index');

    // ------------------ videos
    Route::resource('videos', 'VideoController')->except(['show']);
    Route::get('video/categories', 'VideoController@categories')->name('videos.categories.index');


    // ------------------ categories
    Route::resource('categories', 'CategoryController')->only(['update', 'destroy', 'store']);
    Route::post('categories/sort', 'CategoryController@sort');


    // ------------------ pages
    Route::resource('pages', 'PageController')->except(['show']);

    // ------------------ menus
    Route::resource('menus', 'MenuController')->except(['edit']);
    Route::post('menus/sort', 'MenuController@sort');

    // ------------------ sliders
    Route::resource('sliders', 'SliderController')->except(['show']);
    Route::post('sliders/sort', 'SliderController@sort');

    // ------------------ banners
    Route::resource('banners', 'BannerController')->except(['show']);
    Route::post('banners/sort', 'BannerController@sort');

    // ------------------ links
    Route::resource('links', 'LinkController')->except(['show']);
    Route::post('links/sort', 'LinkController@sort');
    Route::get('links/groups', 'LinkController@groups')->name('links.groups.index');
    Route::put('links/groups/update', 'LinkController@updateGroups')->name('links.groups.update');

    // ------------------ contacts
    Route::resource('contacts', 'ContactController')->only(['index', 'show', 'destroy']);

    // ------------------ newsletters
    Route::resource('newsletters', 'NewsletterController')->only(['index', 'destroy']);
    Route::get('newsletters/export/excel', 'NewsletterController@export')->name('newsletters.export');

    // ------------------ comments
    Route::resource('comments', 'CommentController')->only(['index', 'show', 'destroy', 'update']);
});

// ------------------ Front Part Routes
Route::group(['namespace' => 'Front', 'as' => 'front.'], function () {
    // ------------------ MainController
    Route::get('/', 'MainController@index')->name('index');
    Route::get('/get-new-captcha', 'MainController@captcha');

    // ------------------ pictorials
    Route::resource('pictorials', 'PictorialController')->only(['index', 'show']);
    Route::get('pictorials/category/{category}', 'PictorialController@category')->name('pictorials.category');
    Route::get('pictorials/category-pictorials/{category}', 'PictorialController@categoryPictorials')->name('pictorials.category-pictorials');
    Route::post('pictorials/{pictorial}/comments', 'PictorialController@comments')->name('pictorial.comments');

    // ------------------ posts
    Route::resource('posts', 'PostController')->only(['index', 'show']);
    Route::get('posts/category/{category}', 'PostController@category')->name('posts.category');
    Route::post('posts/{post}/comments', 'PostController@comments')->name('post.comments');

    // ------------------ videos
    Route::resource('videos', 'VideoController')->only(['index', 'show']);
    Route::get('videos/category/{category}', 'VideoController@category')->name('videos.category');
    Route::post('videos/{video}/comments', 'VideoController@comments')->name('video.comments');
    Route::get('pages/{pages}', 'PageController@show')->name('pages.show');

    // ------------------ newsletters
    Route::resource('newsletters', 'NewsletterController')->only(['store']);

    // ------------------ contacts
    Route::resource('contact', 'ContactController')->only(['index', 'store']);
});

Route::get('/about', function () {
    return view('front.about');
});