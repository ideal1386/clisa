<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use sluggable;

    protected $guarded = ['id'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class)->with('categories');
    }

    public function allChildCategories()
    {
        $categories = $this->categories()->pluck('id')->toArray();
        $categories[] = $this->id;

        foreach ($this->categories as $category) {
            $categories = array_merge($categories, $category->allChildCategories());
        }

        return $categories;
    }

    public function getFullTitleAttribute()
    {
        if ($this->category_id) {
            $category_id = $this->category_id;
            $value = [$this->title];
            do {
                $mother = Category::find($category_id);
                $value[] = $mother->title;
                $category_id = $mother->category_id;
            } while ($category_id);

            $value = array_reverse($value);

            return implode(' - ', $value);
        }

        return $this->title;
    }

    public function getLinkAttribute()
    {
        switch ($this->type) {
            case 'productcat': {
                return route('front.products.category', ['category' => $this]);
            }
            case 'postcat': {
                return route('front.posts.category', ['category' => $this]);
            }
            case 'pictorialcat': {
                return route('front.pictorials.category', ['category' => $this]);
            }
            case 'advertisingcat': {
                return route('front.advertisings.category', ['category' => $this]);
            }
            case 'videocat': {
                return route('front.videos.category', ['category' => $this]);
            }
        }
    }

    public function posts()
    {
        if($this->type == 'postcat') {
            return $this->hasMany(Post::class, 'category_id');
        }
    }

    public function videos()
    {
        if($this->type == 'videocat') {
            return $this->hasMany(Video::class, 'category_id');
        }
    }

    public function pictorials()
    {
        if($this->type == 'pictorialcat') {
            return $this->hasMany(Pictorial::class, 'category_id');
        }

        return null;
    }

}
