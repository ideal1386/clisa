<?php

namespace App\Http\Controllers\Front;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::where('lang', app()->getLocale())->where('published', true)->latest()->paginate(9);

        return view('front.posts.index', compact('posts'));
    }

    public function category(Category $category)
    {

        if ($category->type != 'postcat') {
            abort(404);
        }

        $posts = Post::where('published', true)->whereIn('category_id', $category->allChildCategories())->latest()->paginate(9);

        return view('front.posts.category', compact('posts', 'category'));
    }

    public function show(Post $post)
    {
        $comments_count = $post->comments()->where('status', 'accepted')->count();

        $post->load(['comments' => function ($query) {
            $query->whereNull('comment_id')->where('status', 'accepted');
        }]);

        $post->update([
            'view' => $post->view + 1
        ]);

        $latest_posts    = Post::latest()->take(5)->get();
        $most_view_posts = Post::orderBy('view', 'desc')->take(5)->get();

        return view('front.posts.show', compact('post', 'comments_count', 'most_view_posts', 'latest_posts'));
    }

    public function comments(Post $post, Request $request)
    {
        $this->validate($request, [
            'name'       => 'required|string|max:191',
            'email'      => 'required|string|max:191',
            'body'       => 'required|string|max:1000',
            'captcha'    => ['required', 'captcha'],
            'comment_id' => [
                'nullable',
                Rule::exists('comments', 'id')->where(function ($query) {
                    $query->where('comment_id', null);
                }),
            ],
        ]);

        $comment = $post->comments()->create([
            'name'       => $request->name,
            'email'      => $request->email,
            'body'       => $request->body,
            'lang'       => app()->getLocale(),
            'comment_id' => $request->comment_id,
        ]);

        if (auth()->check() && auth()->user()->isAdmin()) {
            $comment->update([
                'status' => 'accepted'
            ]);
        }

        return response('success');
    }
}
