<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required|string|max:190',
            'email' => 'required|email|max:190',
            'captcha' => ['required', 'captcha']
        ]);

        Newsletter::updateOrCreate(
            [
                'email' => $request->email
            ],
            [
                'name' => $request->name
            ]
        );
        
        return response('success');
    }
}
