<?php

namespace App\Http\Controllers\Front;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Notifications\ContactCreated;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ContactController extends Controller
{
    public function index()
    {
        return view('front.contact');
    }

    public function store(Request $request)
    {        
        $this->validate($request, [
            'name'    => 'required|string|max:191',
            'email'   => 'required|string|email|max:191',
            'subject' => 'required|string|max:191',
            'message' => 'required|string|max:2000',
            'captcha' => ['required', 'captcha']
        ]);

        $contact = Contact::create([
            'name'    => $request->name,
            'email'   => $request->email,
            'subject' => $request->subject,
            'message' => $request->message,
        ]);

        $admins = User::where('level', 'admin')->get();
        Notification::send($admins, new ContactCreated($contact));

        return response(['message' => 'success', 'captcha' => captcha_src('flat')]);
    }
}
