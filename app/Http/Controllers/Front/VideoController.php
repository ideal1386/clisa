<?php

namespace App\Http\Controllers\Front;

use App\Category;
use App\Http\Controllers\Controller;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class VideoController extends Controller
{
    public function index()
    {
        $videos = Video::where('lang', app()->getLocale())->where('published', true)->latest()->paginate(9);

        return view('front.videos.index', compact('videos'));
    }

    public function category(Category $category)
    {

        if ($category->type != 'videocat') {
            abort(404);
        }

        $videos = Video::where('published', true)->whereIn('category_id', $category->allChildCategories())->latest()->paginate(9);

        return view('front.videos.category', compact('videos', 'category'));
    }

    public function show(Video $video)
    {
        $comments_count = $video->comments()->where('status', 'accepted')->count();

        $video->load(['comments' => function ($query) {
            $query->whereNull('comment_id')->where('status', 'accepted');
        }]);

        $video->update([
            'view' => $video->view + 1
        ]);

        $latest_videos    = Video::where('lang', app()->getLocale())->where('published', true)->where('id', '!=', $video->id)->latest()->take(3)->get();

        return view('front.videos.show', compact('video', 'comments_count', 'latest_videos'));
    }

    public function comments(Video $video, Request $request)
    {
        $this->validate($request, [
            'name'       => 'required|string|max:191',
            'email'      => 'required|string|max:191',
            'body'       => 'required|string|max:1000',
            'captcha'    => ['required', 'captcha'],
            'comment_id' => [
                'nullable',
                Rule::exists('comments', 'id')->where(function ($query) {
                    $query->where('comment_id', null);
                }),
            ],
        ]);

        $comment = $video->comments()->create([
            'name'       => $request->name,
            'email'      => $request->email,
            'body'       => $request->body,
            'lang'       => app()->getLocale(),
            'comment_id' => $request->comment_id,
        ]);

        if (auth()->check() && auth()->user()->isAdmin()) {
            $comment->update([
                'status' => 'accepted'
            ]);
        }

        return response('success');
    }
}
