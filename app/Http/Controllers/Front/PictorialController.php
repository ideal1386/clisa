<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Pictorial;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PictorialController extends Controller
{
    public function index()
    {
        $categories = Category::where('lang', app()->getLocale())->whereNull('Category_id')->where('type', 'pictorialcat')->orderBy('ordering')->get();
        $pictorials = Pictorial::where('published', true)->latest()->paginate(12);

        return view('front.pictorials.index', compact('categories', 'pictorials'));
    }

    public function category(Category $category)
    {
        if ($category->type != 'pictorialcat') {
            abort(404);
        }

        $pictorials = Pictorial::where('published', true)->whereIn('category_id', $category->allChildCategories())->latest()->paginate(12);
        
        return view('front.pictorials.category', compact('pictorials', 'category'));
    }

    public function show(Pictorial $pictorial)
    {
        if ($pictorial->category) {
            $related_pictorials = Pictorial::where('id', '!=', $pictorial->id)->where('category_id', $pictorial->category->id)->latest()->take(3)->get();
        } else {
            $related_pictorials = Pictorial::where('id', '!=', $pictorial->id)->whereNull('category_id')->latest()->take(3)->get();
        }

        $pictorial->load(['comments' => function ($query) {
            $query->whereNull('comment_id')->where('status', 'accepted')->latest();
        }]);

        return view('front.pictorials.show', compact('pictorial', 'related_pictorials'));
    }

    public function comments(Pictorial $pictorial, Request $request)
    {
        $this->validate($request, [
            'name'       => 'required|string|max:191',
            'email'      => 'required|string|max:191',
            'body'       => 'required|string|max:1000',
            'captcha'    => ['required', 'captcha'],
            'comment_id' => [
                'nullable',
                Rule::exists('comments', 'id')->where(function ($query) {
                    $query->where('comment_id', null);
                }),
            ],
        ]);

        $comment = $pictorial->comments()->create([
            'name'       => $request->name,
            'email'      => $request->email,
            'body'       => $request->body,
            'lang'       => app()->getLocale(),
            'comment_id' => $request->comment_id,
        ]);

        if (auth()->check() && auth()->user()->isAdmin()) {
            $comment->update([
                'status' => 'accepted'
            ]);
        }

        return response('success');
    }
}
