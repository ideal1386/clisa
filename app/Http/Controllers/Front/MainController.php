<?php

namespace App\Http\Controllers\Front;

use App\Advertising;
use App\Http\Controllers\Controller;
use App\Pictorial;
use App\Post;
use App\Slider;
use App\Video;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $latest_posts     = Post::where('lang', app()->getLocale())->where('published', true)->latest()->take(3)->get();
        $latest_videos    = Video::where('lang', app()->getLocale())->where('published', true)->latest()->take(3)->get();

        $main_sliders     = Slider::where('lang', app()->getLocale())->where('group', 1)->where('published', true)->orderBy('ordering')->get();
        $service_sliders  = Slider::where('lang', app()->getLocale())->where('group', 2)->where('published', true)->orderBy('ordering')->get();

        $latest_pictorials       = Pictorial::where('lang', app()->getLocale())->where('published', true)->latest()->take(3)->get();

        return view('front.index', compact(
            'main_sliders',
            'service_sliders',
            'latest_posts',
            'latest_videos',
            'latest_pictorials'
        ));
    }

    public function captcha()
    {
        return response(['captcha' => captcha_src('flat')]);
    }
}
