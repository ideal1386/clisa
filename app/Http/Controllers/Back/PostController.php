<?php

namespace App\Http\Controllers\Back;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function __construct()
    {
        // $this->authorizeResource(Post::class, 'post');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('lang', app()->getLocale())->latest()->paginate(10);

        return view('back.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('lang', app()->getLocale())->where('type', 'postcat')->orderBy('ordering')->get();

        return view('back.posts.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'category_id' => 'required|exists:categories,id',
            'image'       => 'image',
        ]);

        $post = Post::create([
            'title'       => $request->title,
            'content'     => $request->content,
            'category_id' => $request->category_id,
            'published'   => $request->published ? true : false,
            'lang'        => app()->getLocale(),
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $name = uniqid() . '_' . $post->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('posts', $name);

            $post->image = '/uploads/posts/' . $name;
            $post->save();
        }

        if ($request->tags) {
            //add tags and get id
            $tags = addTags($request->tags);
            $post->tags()->attach($tags);
        }

        toastr()->success('نوشته با موفقیت ایجاد شد.');

        return response("success", 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::where('lang', app()->getLocale())->where('type', 'postcat')->orderBy('ordering')->get();

        return view('back.posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'category_id' => 'required|exists:categories,id',
            'image'       => 'image',
        ]);

        $post->update([
            'title'       => $request->title,
            'content'     => $request->content,
            'category_id' => $request->category_id,
            'published'   => $request->published ? true : false,
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $name = uniqid() . '_' . $post->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('posts', $name);

            Storage::disk('public')->delete($post->image);
            $post->image = '/uploads/posts/' . $name;
            $post->save();
        }

        if ($request->tags) {
            //add tags and get id
            $tags = addTags($request->tags);
            $post->tags()->sync($tags);
        }

        toastr()->success('نوشته با موفقیت ویرایش شد.');

        return response("success", 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        Storage::disk('public')->delete($post->image);
        $post->delete();
    }

    //------------- Category methods

    public function categories()
    {
        $categories = Category::where('lang', app()->getLocale())->where('type', 'postcat')->whereNull('category_id')
            ->with('childrenCategories')
            ->orderBy('ordering')
            ->get();

        return view('back.posts.categories', compact('categories'));
    }
}
