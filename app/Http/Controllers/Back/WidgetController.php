<?php

namespace App\Http\Controllers\Back;

use App\Category;
use App\Http\Controllers\Controller;
use App\Widget;
use Illuminate\Http\Request;

class WidgetController extends Controller
{
    public function index()
    {
        $postcats  = Category::where('lang', app()->getLocale())->where('type', 'postcat')->get();
        $videocats = Category::where('lang', app()->getLocale())->where('type', 'videocat')->get();

        return view('back.widgets.index', compact('postcats', 'videocats'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        switch ($request->name) {
            case "index-up-left-sidebar":
            case "index-recent-events":
            case "index-down-left-sidebar":
            case "index-down-right-sidebar": {

                    $request->validate([
                        'title'       => 'required',
                        'category_id' => 'required|exists:categories,id',
                    ]);

                    Widget::updateOrCreate(
                        [
                            'name' => $request->name,
                            'lang' => app()->getLocale(),
                        ],
                        [
                            'title' => $request->title,
                            'options->category_id' => $request->category_id
                        ]
                    );

                    break;
                }
            case "index-down-left-video":
            case "index-down-right-video": {

                    $request->validate([
                        'title'       => 'required',
                        'category_id' => 'required|exists:categories,id',
                    ]);

                    Widget::updateOrCreate(
                        [
                            'name' => $request->name,
                            'lang' => app()->getLocale(),
                        ],
                        [
                            'title' => $request->title,
                            'options->category_id' => $request->category_id
                        ]
                    );

                    break;
                }
        }

        return response('success');
    }
}
