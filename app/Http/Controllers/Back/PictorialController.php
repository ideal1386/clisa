<?php

namespace App\Http\Controllers\Back;

use App\Category;
use App\Http\Controllers\Controller;
use App\Pictorial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PictorialController extends Controller
{
    public function index(Request $request)
    {
        $paginate = $request->paginate;
        $paginate = ($paginate && is_numeric($paginate)) ? $paginate : 10;

        $pictorials   = Pictorial::where('lang', app()->getLocale())->filter($request)->paginate($paginate);
        $categories   = Category::where('lang', app()->getLocale())->where('type', 'pictorialcat')->orderBy('ordering')->get();

        return view('back.pictorials.index', compact('pictorials', 'categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'category_id' => 'required|exists:categories,id',
            'image'       => 'image',
        ]);

        $pictorial = Pictorial::create([
            'title'          => $request->title,
            'lang'           => app()->getLocale(),
            'category_id'    => $request->category_id,
            'content'        => $request->content,
            'published'      => $request->published ? true : false,
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $name = uniqid() . '_' . $pictorial->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('pictorials', $name);

            $pictorial->image = '/uploads/pictorials/' . $name;
            $pictorial->save();
        }

        if ($request->images) {
            $images = explode(',', $request->images);

            foreach ($images as $image) {

                if (Storage::exists('tmp/' . $image)) {

                    Storage::move('tmp/' . $image, 'pictorials/' . $image);

                    $pictorial->gallery()->create([
                        'image' => '/uploads/pictorials/' . $image,
                    ]);
                }
            }
        }

        if ($request->tags) {
            //add tags and get id
            $tags = addTags($request->tags);
            $pictorial->tags()->attach($tags);
        }

        toastr()->success('به روایت تصویر با موفقیت ایجاد شد.');

        return response("success", 200);
    }

    public function create()
    {
        $categories = Category::where('lang', app()->getLocale())->where('type', 'pictorialcat')->orderBy('ordering')->get();

        return view('back.pictorials.create', compact(
            'categories'
        ));
    }

    public function edit(Pictorial $pictorial)
    {
        $categories =  Category::where('lang', app()->getLocale())->where('type', 'pictorialcat')->orderBy('ordering')->get();

        return view('back.pictorials.edit', compact(
            'pictorial',
            'categories'
        ));
    }

    public function update(Request $request, Pictorial $pictorial)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'category_id' => 'required|exists:categories,id',
            'image'       => 'image',
        ]);

        $pictorial->update([
            'title'           => $request->title,
            'category_id'     => $request->category_id,
            'content'         => $request->content,
            'published'       => $request->published ? true : false,
        ]);

        if ($request->hasFile('image')) {
            if ($pictorial->image && Storage::disk('public')->exists($pictorial->image)) {
                Storage::disk('public')->delete($pictorial->image);
            }

            $file = $request->image;
            $name = uniqid() . '_' . $pictorial->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('pictorials', $name);

            $pictorial->image = '/uploads/pictorials/' . $name;
            $pictorial->save();
        }


        $pictorial_images = $pictorial->gallery()->pluck('image')->toArray();
        $images         = explode(',', $request->images);
        $deleted_images = array_diff($pictorial_images, $images);


        foreach ($deleted_images as $del_img) {
            $del_img = $pictorial->gallery()->where('image', $del_img)->first();

            if (!$del_img) {
                continue;
            }

            if (Storage::disk('public')->exists($del_img)) {
                Storage::disk('public')->delete($del_img);
            }

            $del_img->delete();
        }

        if ($request->images) {

            foreach ($images as $image) {

                if (Storage::exists('tmp/' . $image)) {

                    Storage::move('tmp/' . $image, 'pictorials/' . $image);

                    $pictorial->gallery()->create([
                        'image' => '/uploads/pictorials/' . $image,
                    ]);
                }
            }
        }

        if ($request->tags) {
            //add tags and get id
            $tags = addTags($request->tags);
            $pictorial->tags()->sync($tags);
        }

        toastr()->success('به روایت تصویر با موفقیت ویرایش شد.');

        return response("success", 200);
    }

    public function image_store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file|image|mimes:jpeg,png,jpg|max:10240',
        ]);

        $image = $request->file('file');

        $currentDate = Carbon::now()->toDateString();
        $imagename = 'img' . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

        $image->storeAs('tmp', $imagename);

        return response()->json(['imagename' => $imagename]);
    }

    public function image_delete(Request $request)
    {
        $filename = $request->get('filename');

        if (Storage::exists('tmp/' . $filename)) {
            Storage::delete('tmp/' . $filename);
        }

        return response('success');
    }

    public function destroy(Pictorial $pictorial)
    {
        $pictorial->tags()->detach();

        if ($pictorial->image && Storage::disk('public')->exists($pictorial->image)) {
            Storage::disk('public')->delete($pictorial->image);
        }

        foreach ($pictorial->gallery as $image) {
            if (Storage::disk('public')->exists($image->image)) {
                Storage::disk('public')->delete($image->image);
            }

            $image->delete();
        }

        $pictorial->delete();

        return response('success');
    }

    //------------- Category methods

    public function categories()
    {
        $categories = Category::where('lang', app()->getLocale())->where('type', 'pictorialcat')->whereNull('category_id')
            ->with('childrenCategories')
            ->orderBy('ordering')
            ->get();

        return view('back.pictorials.categories', compact('categories'));
    }
}
