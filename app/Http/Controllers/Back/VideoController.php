<?php

namespace App\Http\Controllers\Back;

use App\Video;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function __construct()
    {
        // $this->authorizeResource(Video::class, 'video');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::where('lang', app()->getLocale())->latest()->paginate(10);

        return view('back.videos.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('lang', app()->getLocale())->where('type', 'videocat')->orderBy('ordering')->get();

        return view('back.videos.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'category_id' => 'required|exists:categories,id',
            'image'       => 'image',
            'video_type'  => 'required',
        ]);

        if ($request->video_type == 'locale-video') {
            $request->validate([
                'video' => 'required|mimes:mp4,mov,ogg,qt'
            ]);
        } else {
            $request->validate([
                'youtube' => 'required|string'
            ]);
        }

        $video = Video::create([
            'title'       => $request->title,
            'content'     => $request->content,
            'category_id' => $request->category_id,
            'youtube'     => $request->youtube,
            'video_type'  => $request->video_type,
            'published'   => $request->published ? true : false,
            'lang'        => app()->getLocale(),
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $name = uniqid() . '_' . $video->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('videos', $name);

            $video->image = '/uploads/videos/' . $name;
        }

        if ($request->hasFile('video')) {
            $file = $request->video;
            $name = uniqid() . '_' . $video->id . '.' . $file->getClientOriginalExtension();
            $request->video->storeAs('videos', $name);

            $video->video = '/uploads/videos/' . $name;
        }

        $video->save();

        if ($request->tags) {
            //add tags and get id
            $tags = addTags($request->tags);
            $video->tags()->attach($tags);
        }

        toastr()->success('ویدیو با موفقیت ایجاد شد.');

        return response("success", 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        $categories = Category::where('lang', app()->getLocale())->where('type', 'videocat')->orderBy('ordering')->get();

        return view('back.videos.edit', compact('video', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'category_id' => 'required|exists:categories,id',
            'image'       => 'image',
            'video_type'  => 'required',
        ]);

        if ($request->video_type == 'locale-video' && !$video->video) {
            $request->validate([
                'video' => 'required|mimes:mp4,mov,ogg,qt'
            ]);
        } else if ($request->video_type == 'youtube-video' && !$video->youtube) {
            $request->validate([
                'youtube' => 'required|string'
            ]);
        }

        $video->update([
            'title'       => $request->title,
            'content'     => $request->content,
            'category_id' => $request->category_id,
            'youtube'     => $request->youtube,
            'video_type'  => $request->video_type,
            'published'   => $request->published ? true : false,
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $name = uniqid() . '_' . $video->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('videos', $name);

            Storage::disk('public')->delete($video->image);
            $video->image = '/uploads/videos/' . $name;
        }

        if ($request->hasFile('video')) {
            $file = $request->video;
            $name = uniqid() . '_' . $video->id . '.' . $file->getClientOriginalExtension();
            $request->video->storeAs('videos', $name);

            Storage::disk('public')->delete($video->video);
            $video->video = '/uploads/videos/' . $name;
        }

        $video->save();

        if ($request->tags) {
            //add tags and get id
            $tags = addTags($request->tags);
            $video->tags()->sync($tags);
        }

        toastr()->success('ویدیو با موفقیت ویرایش شد.');

        return response("success", 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        Storage::disk('public')->delete($video->image);
        $video->delete();
    }

    //------------- Category methods

    public function categories()
    {
        $categories = Category::where('lang', app()->getLocale())->where('type', 'videocat')->whereNull('category_id')
            ->with('childrenCategories')
            ->orderBy('ordering')
            ->get();

        return view('back.videos.categories', compact('categories'));
    }
}
