<?php

namespace App\Http\Controllers\Back;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public $ordering = 1;

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'type'  => 'required|string',
        ]);

        $category = Category::create([
            'title' => $request->title,
            'type'  => $request->type,
            'lang'  => app()->getLocale()
        ]);

        return $category;
    }
    
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|string',
            'image' => 'image',
        ]);

        $category->update([
            'title' => $request->title,
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $name = uniqid() . '_' . $category->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('categories', $name);

            if ($category->image) {
                Storage::disk('public')->delete($category->image);
            }

            $category->image = '/uploads/categories/' . $name;
            $category->save();
        }

        return $category->title;
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        $category->delete();
    }

    public function sort(Request $request)
    {
        $this->validate($request, [
            'categories' => 'required|array'
        ]);

        $categories = $request->categories;

        $this->sort_category($categories);

        return 'success';
    }

    private function sort_category($categories, $category_id = null)
    {
        foreach ($categories as $category) {
            Category::find($category['id'])->update(['category_id' => $category_id, 'ordering' => $this->ordering++]);
            if (array_key_exists('children', $category)) {
                $this->sort_category($category['children'], $category['id']);
            }
        }
    }
}
