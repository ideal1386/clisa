<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Link;
use App\Menu;
use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        $pages = Page::where('lang', app()->getLocale())->latest()->paginate(10);

        return view('back.pages.index', compact('pages'));
    }

    public function create()
    {
        return view('back.pages.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'content'     => 'required',
        ]);

        $page = Page::create([
            'title'       => $request->title,
            'lang'        => app()->getLocale(),
            'content'     => $request->content,
            'published'   => $request->published ? true : false,
        ]);

        if ($request->tags) {
            //add tags and get id
            $tags = addTags($request->tags);
            $page->tags()->attach($tags);
        }

        toastr()->success('صفحه با موفقیت ایجاد شد.');

        return response("success", 200);
    }

    public function edit(Page $page)
    {
        return view('back.pages.edit', compact('page'));
    }

    public function update(Request $request, Page $page)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'content'     => 'required',
        ]);

        $slug = $page->slug;

        $page->update([
            'title'       => $request->title,
            'content'     => $request->content,
            'published'   => $request->published ? true : false,
        ]);

        Menu::where('link', '/pages/' . $slug)->update([
            'link' => '/pages/' . $page->slug,
        ]);

        Link::where('link', '/pages/' . $slug)->update([
            'link' => '/pages/' . $page->slug,
        ]);

        if ($request->tags) {
            //add tags and get id
            $tags = addTags($request->tags);
            $page->tags()->sync($tags);
        }

        toastr()->success('صفحه با موفقیت ویرایش شد.');

        return response("success", 200);
    }

    public function destroy(Page $page)
    {
        $page->delete();
        
        return response("success", 200);
    }
}
