<?php

namespace App\Http\Controllers\Back;

use App\Category;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public $ordering = 1;

    public function index()
    {
        $menus = Menu::where('lang', app()->getLocale())->whereNull('menu_id')
            ->with('childrenMenus')
            ->orderBy('ordering')
            ->get();

        $categories = Category::where('lang', app()->getLocale())->orderBy('ordering')->get()->groupBy('type');

        return view('back.menus.index', compact('menus', 'categories'));
    }

    public function show(Menu $menu)
    {
        return response()->json(['menu' => $menu, 'title' => $menu->full_title]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
        ]);

        $ordering = Menu::max('ordering') + 1;

        $type = $request->type;

        switch ($type) {
            case 'category': {
                    $this->validate($request, [
                        'category' => 'required|exists:categories,id',
                    ]);

                    $menu = Menu::create([
                        'lang'        => app()->getLocale(),
                        'ordering'    => $ordering,
                        'menuable_id' => $request->category,
                        'type'        => $type,
                    ]);

                    break;
                }
            case 'static': {
                    $this->validate($request, [
                        'title'  => 'required',
                        'static' => 'required',
                    ]);

                    $menu = Menu::create([
                        'lang'        => app()->getLocale(),
                        'ordering'    => $ordering,
                        'title'       => $request->title,
                        'static_type' => $request->static,
                        'type'        => $type,
                    ]);

                    break;
                }
            case 'normal': {
                    $this->validate($request, [
                        'title' => 'required',
                        'link'  => 'required',
                    ]);

                    $menu = Menu::create([
                        'lang'        => app()->getLocale(),
                        'ordering' => $ordering,
                        'title' => $request->title,
                        'link'  => $request->link,
                        'type'  => $type,
                    ]);
                    break;
                }
        }

        return response()->json(['menu' => $menu, 'title' => $menu->full_title]);
    }

    public function update(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'type' => 'required',
        ]);

        $type = $request->type;

        switch ($type) {
            case 'category': {
                    $this->validate($request, [
                        'category' => 'required|exists:categories,id',
                    ]);

                    $menu->update([
                        'menuable_id' => $request->category,
                        'title' => null,
                        'link' => null,
                        'type' => $type,
                    ]);

                    break;
                }
            case 'static': {
                    $this->validate($request, [
                        'title'  => 'required',
                        'static' => 'required',
                    ]);

                    $menu->update([
                        'title'       => $request->title,
                        'static_type' => $request->static,
                        'type'        => $type,
                    ]);

                    break;
                }
            case 'normal': {
                    $this->validate($request, [
                        'title' => 'required',
                        'link' => 'required',
                    ]);

                    $menu->update([
                        'title' => $request->title,
                        'link' => $request->link,
                        'menuable_id' => null,
                        'type' => $type,
                    ]);

                    break;
                }
        }

        return response()->json(['menu' => $menu, 'title' => $menu->full_title]);
    }

    public function destroy(Menu $menu)
    {
        $menu->delete();
    }

    public function sort(Request $request)
    {
        $this->validate($request, [
            'menus' => 'required|array',
        ]);

        $menus = $request->menus;

        $this->sort_category($menus);

        return 'success';
    }

    private function sort_category($menus, $menu_id = null)
    {
        foreach ($menus as $category) {
            Menu::find($category['id'])->update(['menu_id' => $menu_id, 'ordering' => $this->ordering++]);
            if (array_key_exists('children', $category)) {
                $this->sort_category($category['children'], $category['id']);
            }
        }
    }
}
