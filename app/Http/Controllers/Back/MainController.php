<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class MainController extends Controller
{
    public function index()
    {
        $users_count    = User::where('level', '!=', 'creator')->count();

        return view('back.index', compact('users_count'));
    }

    public function settings()
    {
        return view('back.settings');
    }

    public function profile(Request $request)
    {
        $user = auth()->user();

        $this->validate($request, [
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'username' => 'required|string|max:191',
        ]);

        if ($request->password || $request->password_confirmation) {
            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required',
            ]);

            $user->password = Hash::make($request->password);
        }

        if ($request->hasFile('image')) {
            $this->validate($request, [
                'image' => 'image|max:2048',
            ]);

            $imageName = time() . '_' . $user->id . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/users/'), $imageName);

            if ($user->image && file_exists(public_path($user->image))) {
                unlink(public_path($user->image));
            }

            $user->image = '/uploads/users/' . $imageName;
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->username = $request->username;
        $user->save();

        return response()->json('success');
    }

    public function socials(Request $request)
    {
        $socials = $request->all();

        foreach ($socials as $social => $value) {
            option_update($social, $value);
        }
    }

    public function gateways(Request $request)
    {
        $gateways = $request->except(['mellat', 'payir']);

        if ($request->payir) {
            $request->validate([
                'gateway_payir_api' => 'required',
            ]);

            option_update('gateway_payir_status', 'on');

        } else {
            option_update('gateway_payir_status', 'off');
        }

        if ($request->mellat) {
            $request->validate([
                'gateway_mellat_username'   => 'required',
                'gateway_mellat_password'   => 'required',
                'gateway_mellat_terminalId' => 'required',
            ]);

            option_update('gateway_mellat_status', 'on');

        } else {
            option_update('gateway_mellat_status', 'off');
        }

        foreach ($gateways as $gateway => $value) {
            option_update($gateway, $value);
        }
    }

    public function sizes(Request $request)
    {
        $sizes = $request->all();

        foreach ($sizes as $size => $value) {
            option_update($size, $value);
        }
    }

    public function information(Request $request)
    {
        $informations = $request->except(['info_icon', 'info_logo']);

        $this->validate($request, [
            'info_site_title' => 'required',
            'info_icon' => 'image|max:2048',
            'info_logo' => 'image|max:2048',
            'info_city_id' => 'exists:cities,id',
            'info_province_id' => 'exists:provinces,id',
        ]);

        if ($request->hasFile('info_icon')) {

            $imageName = time() . '_icon.' . $request->info_icon->getClientOriginalExtension();
            $request->info_icon->move(public_path('uploads/'), $imageName);

            $old_icon = option('info_icon');

            if ($old_icon) {
                Storage::disk('public')->delete($old_icon);
            }

            $informations['info_icon'] = '/uploads/' . $imageName;
        }

        if ($request->hasFile('info_logo')) {

            $imageName = time() . '_logo.' . $request->info_logo->getClientOriginalExtension();
            $request->info_logo->move(public_path('uploads/'), $imageName);

            $old_logo = option('info_logo');

            if ($old_logo) {
                Storage::disk('public')->delete($old_logo);
            }

            $informations['info_logo'] = '/uploads/' . $imageName;
        }

        foreach ($informations as $information => $value) {
            option_update($information, $value);
        }
    }

    public function get_tags(Request $request)
    {
        $tags = Tag::where('name', 'like', '%' . $request->term . '%')->latest()->take(5)->pluck('name')->toArray();

        return response()->json($tags);
    }

    public function login()
    {
        return view('back.auth.login');
    }

    public function notifications()
    {
        $notifications = auth()->user()->notifications()->paginate(15);

        auth()->user()->unreadNotifications->markAsRead();

        return view('back.notifications', compact('notifications'));
    }

    public function fileManager()
    {
        return view('back.file-manager');
    }

    public function fileManagerIframe()
    {
        return view('back.file-manager-iframe');
    }
}
