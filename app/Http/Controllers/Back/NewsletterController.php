<?php

namespace App\Http\Controllers\Back;

use App\Exports\NewslettersExport;
use App\Http\Controllers\Controller;
use App\Newsletter;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function index()
    {
        $newsletters = Newsletter::latest()->paginate(15);

        return view('back.newsletters.index', compact('newsletters'));
    }

    public function destroy(Newsletter $newsletter)
    {
        $newsletter->delete();

        return response('success');
    }

    public function export() 
    {
        return Excel::download(new NewslettersExport, 'newsletters.xlsx');
    }
}
