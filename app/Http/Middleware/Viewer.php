<?php

namespace App\Http\Middleware;

use Closure;
use App\Viewer as ViewerModel;
use Jenssegers\Agent\Agent;

class Viewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->method() == 'GET') {
            $agent = new Agent();

            ViewerModel::create([
                'ip'   => request()->ip(),
                'path' => request()->getRequestUri(),
                'auth' => auth()->check(),

                'options->device'   => $agent->device(),
                'options->platform' => $agent->platform(),
                'options->browser'  => $agent->browser(),
                'options->robot'    => $agent->robot(),
                'options->method'   => request()->method(),
                'options->referer'   => request()->headers->get('referer'),
            ]);
        }

        return $next($request);
    }
}
