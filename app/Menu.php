<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded = ['id'];

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function childrenmenus()
    {
        return $this->hasMany(Menu::class)->with('menus');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'menuable_id', 'id');
    }

    public function getFullTitleAttribute()
    {
        switch ($this->type) {
            case 'category': {
                    $title = $this->category->full_title . ' ( دسته بندی  )';

                    break;
                }

            case 'static': {
                    $title = $this->title . ' ( منو ثابت )';

                    break;
                }
            default: {
                    $title = $this->title;
                    break;
                }
        }

        return $title;
    }
}
