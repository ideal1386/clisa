<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $guarded = ['id'];

    public function group()
    {
        return $this->belongsTo(LinkGroup::class, 'link_group_id');
    }
}
