<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Pictorial extends Model
{
    use sluggable;

    protected $guarded = ['id'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public function getGetTagsAttribute()
    {
        return implode(',', $this->tags()->pluck('name')->toArray());
    }

    public function shortContent($words = 15)
    {
        $content = strip_tags($this->content);

        return Str::words($content, $words);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->title) {
            $query->where('title', 'like', '%' . $request->title . '%');
        }

        $categories = $request->category_id;

        if ($categories) {

            $allcats = $categories;

            foreach ($categories as $category) {

                $category = Category::find($category);

                if ($category) {
                    $allcats = array_merge($category->allChildCategories(), $allcats);
                }
            }

            $query->whereIn('category_id', $allcats);
        }

        switch ($request->ordering) {
            case 'oldest': {
                    $query->oldest();
                    break;
                }
            default: {
                    $query->latest();
                }
        }

        return $query;
    }

    public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleryable');
    }

    public function link()
    {
        return route('front.pictorials.show', ['pictorial' => $this]);
    }
}
