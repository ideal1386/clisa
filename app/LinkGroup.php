<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkGroup extends Model
{
    protected $guarded = ['id'];
    
    public function links()
    {
        return $this->hasMany(Link::class);
    }
}
