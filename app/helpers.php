<?php

// helper functions

use App\Banner;
use App\Cart;
use App\Option;
use App\Specification;
use App\SpecificationGroup;
use App\SpecType;
use App\Tag;
use App\Viewer;
use App\Widget;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

/* add active class to li */

function active_class($route_name)
{

    $current_route = Request::route()->getName();

    return ($current_route == $route_name) ? 'active' : '';
}

function open_class($route_list)
{
    $text = '';

    $current_route = Request::route()->getName();

    foreach ($route_list as $route) {
        if ($current_route == $route) {
            $text = 'open';
            break;
        }
    }

    return $text;
}

function option_update($option_name, $option_value)
{
    $option = Option::firstOrNew(['option_name' => $option_name, 'lang' => app()->getLocale()]);

    $option->option_value = $option_value;
    $option->save();
}

function option($option_name, $default_value = '')
{
    $option = Option::where('option_name', $option_name)->where('lang', app()->getLocale())->first();

    return $option ? $option->option_value : $default_value;
}

// add new tags and return tags id
function addTags($tags)
{
    $tags = explode(',', $tags);
    $tags_id = [];

    foreach ($tags as $item) {
        $tag = Tag::where('name', $item)->first();
        if (!$tag) {
            $tag = Tag::create([
                'name' => $item,
            ]);
        }
        $tags_id[] = $tag->id;
    }

    return $tags_id;
}

function get_cart()
{
    $cart = null;

    if (auth()->check()) {
        $cart = auth()->user()->cart;
    } else {
        $cart_id = Cookie::get('cart_id');

        if ($cart_id) {
            $cart = Cart::whereNull('user_id')->find($cart_id);
        }
    }

    return $cart;
}

/* return true if cart products quantity is ok
 * and return false if cart products quantity is more than product stock
 */
function check_cart_quantity()
{
    $cart = get_cart();

    if (!$cart || !$cart->products()->count()) {
        return true;
    }

    foreach ($cart->products as $product) {
        if ($product->price_type == "single-price") {
            if ($product->type == 'physical' && ($product->pivot->quantity > $product->stock || ($product->cart_max !== null ? $product->pivot->quantity > $product->cart_max : false))) {
                return false;
            }
        } else {
            $price = $product->prices()->find($product->pivot->price_id);

            if ($product->pivot->quantity > $price->stock || ($price->cart_max !== null ? $product->pivot->quantity > $price->cart_max : false)) {
                return false;
            }
        }
    }

    return true;
}

//get user address 
function user_address($key)
{
    if (old($key)) {
        return old($key);
    }

    return auth()->user()->address ? auth()->user()->address->$key : '';
}

function short_content($str, $words = 20, $strip_tags = true)
{
    if ($strip_tags) {
        $str = strip_tags($str);
    }

    return Str::words($str, $words);
}


function spec_type($request)
{
    if (!$request->spec_type || !$request->specification_group) {
        return null;
    }

    $spec_type = SpecType::firstOrCreate([
        'name' => $request->spec_type
    ]);

    $group_ordering = 0;

    foreach ($request->specification_group as $group) {

        if (!isset($group['specifications'])) {
            continue;
        }

        $spec_group = SpecificationGroup::firstOrCreate([
            'name' => $group['name'],
        ]);

        $specification_ordering = 0;

        foreach ($group['specifications'] as $specification) {
            $spec = Specification::firstOrCreate([
                'name' => $specification['name']
            ]);

            if (!$spec_type->specifications()->where('specification_id', $spec->id)->where('specification_group_id', $spec_group->id)->first()) {
                $spec_type->specifications()->attach([
                    $spec->id => [
                        'specification_group_id' => $spec_group->id,
                        'group_ordering'         => $group_ordering,
                        'specification_ordering' => $specification_ordering++,
                    ]
                ]);
            }
        }

        $group_ordering++;
    }

    return $spec_type->id;
}

function viewers_data($number = 7)
{
    $data = [];

    for ($i = 0; $i < $number; $i++) {
        $date = Carbon::now()->subDays($i);

        $views = Viewer::whereDate('created_at', $date)->count();

        $data[verta($date)->format('l')] = $views;
    }

    return $data;
}

function ip_data($number = 7)
{
    $data = [];

    for ($i = 0; $i < $number; $i++) {
        $date = Carbon::now()->subDays($i);

        $views = Viewer::whereDate('created_at', $date)->get()->unique('ip')->count();

        $data[verta($date)->format('l')] = $views;
    }

    return $data;
}

function array_to_string($array)
{
    $comma_separated = implode("','", $array);
    $comma_separated = "'" . $comma_separated . "'";
    return $comma_separated;
}

function get_discount_price($price, $discount, $number_format = false)
{
    $price = $price - ($price * ($discount / 100));

    return $number_format ? number_format($price) : $price;
}

function get_max_stock($product)
{
    if ($product->price_type == "single-price") {
        return min($product->stock, $product->cart_max);
    } else {
        $price = $product->prices()->find($product->pivot->price_id);

        return min($price->stock, $price->cart_max);
    }
}

function get_langs()
{
    return config('app.locales');
}

function get_current_url($lang)
{
    $locale = request()->segment(1);
    $current_url = request()->url();

    if (!$locale || !array_key_exists($locale, get_langs())) {
        $index = url('/');

        $url = str_replace_first($index, $index . '/' . $lang, $current_url);
    } else {
        $url = str_replace_first($locale, $lang, $current_url);
    }

    return $url;
}

function str_replace_first($from, $to, $content)
{
    $from = '/' . preg_quote($from, '/') . '/';

    return preg_replace($from, $to, $content, 1);
}

function local_info()
{
    $local = app()->getLocale();

    $locals = get_langs();

    return $locals[$local];
}

function get_banners($group_name)
{
    return Banner::where('lang', app()->getLocale())->where('group', $group_name)->orderBy('ordering')->get();
}

function category_group($key)
{
    switch ($key) {
        case 'postcat': {
                return 'دسته بندی وبلاگ';
            }
        case 'advertisingcat': {
                return 'دسته بندی آگهی';
            }
        case 'pictorialcat': {
                return 'دسته بندی به روایت تصویر';
            }
        case 'productcat': {
                return 'دسته بندی محصول';
            }
    }
}

function get_option_value($obj, $property)
{
    $obj = json_decode($obj);

    if (!is_object($obj)) {
        return null;
    }

    if (property_exists($obj, $property)) {
        return $obj->$property;
    }

    return null;
}

function get_widget($name)
{
    $widget = Widget::firstOrCreate([
        'name' => $name,
        'lang' => app()->getLocale(),
    ]);

    return $widget;
}

function youtube_iframe($string) {
	return preg_replace(
		"/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
		"<iframe class=\"youtube\" src=\"//www.youtube.com/embed/$2?rel=0\" allowfullscreen ></iframe>",
		$string
	);
}