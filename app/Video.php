<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Video extends Model
{
    use sluggable;

    protected $guarded = ['id'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public function getGetTagsAttribute()
    {
        return implode(',', $this->tags()->pluck('name')->toArray());
    }

    public function shortContent($words = 15)
    {
        $content = strip_tags($this->content);

        return Str::words($content, $words);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function link()
    {
        return route('front.videos.show', ['video' => $this]);
    }
}
