<?php

namespace App\Exports;

use App\Newsletter;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class NewslettersExport implements FromView
{
    public function view(): View
    {
        return view('back.newsletters.export', [
            'newsletters' => Newsletter::latest()->get()
        ]);
    }
}

