<?php

namespace App\Traits;

use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

trait CommentTrait {
    public function comments($model, Request $request)
    {
        dd($this);
        $this->validate($request, [
            'name'       => 'required|string|max:191',
            'email'      => 'required|string|max:191',
            'body'       => 'required|string|max:1000',
            'comment_id' => [
                'nullable',
                Rule::exists('comments', 'id')->where(function ($query) {
                    $query->where('comment_id', null);
                }),
            ],
        ]);

        $comment = $this->comments()->create([
            'name'       => $request->name,
            'email'      => $request->body,
            'body'       => $request->body,
            'lang'       => app()->getLocale(),
            'comment_id' => $request->comment_id,
        ]);

        if (auth()->user()->isAdmin()) {
            $comment->update([
                'status' => 'accepted'
            ]);
        }
    }
}