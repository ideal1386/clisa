<?php

namespace App\Providers;

use App\Category;
use App\LinkGroup;
use App\Menu;
use App\Page;
use App\Pictorial;
use App\Post;
use App\Video;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (!$this->app->runningInConsole()) {
            $this->viewComposer();
        }
    }

    private function viewComposer()
    {
        // share with all views

        View()->composer('*', function ($view) {
            $current_local = local_info();

            $view->with('current_local', $current_local);
        });

        // SHARE WITH SPECIFIC VIEW

        // -------------- front

        view()->composer(['front.partials.footer'], function ($view) {

            $footer_links     = LinkGroup::where('lang', app()->getLocale())->get();

            $view->with('footer_links', $footer_links);
        });

        view()->composer(['front.partials.menu.menu', 'front.partials.mobile-menu.menu'], function ($view) {

            $productcats    = Category::where('lang', app()->getLocale())->where('type', 'productcat')->whereNull('category_id')->orderBy('ordering')->get();
            $postcats       = Category::where('lang', app()->getLocale())->where('type', 'postcat')->whereNull('category_id')->orderBy('ordering')->get();
            $videocats      = Category::where('lang', app()->getLocale())->where('type', 'videocat')->whereNull('category_id')->orderBy('ordering')->get();
            $pictorialcats  = Category::where('lang', app()->getLocale())->where('type', 'pictorialcat')->whereNull('category_id')->orderBy('ordering')->get();
            $menus          = Menu::where('lang', app()->getLocale())->whereNull('menu_id')->orderBy('ordering')->get();

            $view->with(compact(
                'productcats',
                'postcats',
                'pictorialcats',
                'videocats',
                'menus'
            ));
        });

        view()->composer(['front.partials.sidebar'], function ($view) {

            $latest_posts = Post::where('lang', app()->getLocale())->where('published', true)->latest()->take(4)->get();
            $latest_videos = Video::where('lang', app()->getLocale())->where('published', true)->latest()->take(4)->get();

            $view->with(compact('latest_posts', 'latest_videos'));
        });

        view()->composer(['front.layouts.footer'], function ($view) {

            $latest_pictorials = Pictorial::where('lang', app()->getLocale())->where('published', true)->latest()->take(6)->get();

            $view->with(compact('latest_pictorials'));
        });


        // -------------- back

        view()->composer(['back.partials.notifications', 'back.partials.sidebar'], function ($view) {

            $notifications = auth()->user()->unreadNotifications;

            $view->with('notifications', $notifications);
        });

        view()->composer(['back.menus.index', 'back.sliders.create', 'back.sliders.edit', 'back.banners.create', 'back.banners.edit', 'back.links.create', 'back.links.edit'], function ($view) {

            $pages = Page::pluck('slug');

            $view->with('pages', $pages);
        });
    }
}
