<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'ideal it',
            'last_name'  => 'ideal it',
            'username'   => 'ideal',
            'level'      => 'creator',
            'password'   => bcrypt('I@deal/123+'),
            'created_at' => Carbon::now(),
        ]);

        User::create([
            'first_name' => 'ادمین',
            'last_name'  => 'ادمین',
            'username'   => 'test',
            'level'      => 'admin',
            'password'   => bcrypt(123321),
            'created_at' => Carbon::now(),
        ]);
    }
}
