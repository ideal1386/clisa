<?php

use App\Slider;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ordering = 1;

        if(!Storage::disk('public')->exists('uploads/sliders/1.jpg')) {
            Storage::disk('public')->copy('front/img/main-slider/1.jpg', 'uploads/sliders/1.jpg');
        }
        
        Slider::create([
            'link'        => '#',
            'group'       => 1,
            'published'   => true,
            'image'       => '/uploads/sliders/1.jpg',
            'ordering'    => $ordering++,
            'lang'        => app()->getLocale(),
        ]);

        if(!Storage::disk('public')->exists('uploads/sliders/2.jpg')) {
            Storage::disk('public')->copy('front/img/main-slider/2.jpg', 'uploads/sliders/2.jpg');
        }
        
        Slider::create([
            'link'        => '#',
            'group'       => 1,
            'published'   => true,
            'image'       => '/uploads/sliders/2.jpg',
            'ordering'    => $ordering++,
            'lang'        => app()->getLocale(),
        ]);

        if(!Storage::disk('public')->exists('uploads/sliders/3.jpg')) {
            Storage::disk('public')->copy('front/img/main-slider/slider-responsive/1.jpg', 'uploads/sliders/3.jpg');
        }
        
        Slider::create([
            'link'        => '#',
            'group'       => 3,
            'published'   => true,
            'image'       => '/uploads/sliders/3.jpg',
            'ordering'    => $ordering++,
            'lang'        => app()->getLocale(),
        ]);

        if(!Storage::disk('public')->exists('uploads/sliders/4.jpg')) {
            Storage::disk('public')->copy('front/img/main-slider/slider-responsive/2.jpg', 'uploads/sliders/4.jpg');
        }
        
        Slider::create([
            'link'        => '#',
            'group'       => 3,
            'published'   => true,
            'image'       => '/uploads/sliders/4.jpg',
            'ordering'    => $ordering++,
            'lang'        => app()->getLocale(),
        ]);

        if(!Storage::disk('public')->exists('uploads/sliders/3.png')) {
            Storage::disk('public')->copy('front/img/brand/1.png', 'uploads/sliders/3.png');
        }
        
        Slider::create([
            'link'        => '#',
            'group'       => 2,
            'published'   => true,
            'image'       => '/uploads/sliders/3.png',
            'ordering'    => $ordering++,
            'lang'        => app()->getLocale(),
        ]);

        if(!Storage::disk('public')->exists('uploads/sliders/4.png')) {
            Storage::disk('public')->copy('front/img/brand/2.png', 'uploads/sliders/4.png');
        }
        
        Slider::create([
            'link'        => '#',
            'group'       => 2,
            'published'   => true,
            'image'       => '/uploads/sliders/4.png',
            'ordering'    => $ordering++,
            'lang'        => app()->getLocale(),
        ]);

        if(!Storage::disk('public')->exists('uploads/sliders/5.png')) {
            Storage::disk('public')->copy('front/img/brand/3.png', 'uploads/sliders/5.png');
        }
        
        Slider::create([
            'link'        => '#',
            'group'       => 2,
            'published'   => true,
            'image'       => '/uploads/sliders/5.png',
            'ordering'    => $ordering++,
            'lang'        => app()->getLocale(),
        ]);
    }
}
