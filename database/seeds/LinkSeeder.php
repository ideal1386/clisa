<?php

use App\LinkGroup;
use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (get_langs() as $lang => $info) {
            LinkGroup::create([
                'title' => 'گروه اول',
                'lang' => $lang,
            ]);
    
            LinkGroup::create([
                'title' => 'گروه دوم',
                'lang' => $lang,
            ]);
    
            $groups = LinkGroup::where('lang', $lang);
    
            foreach ($groups as $group) {
                for ($i = 0; $i < 4; $i++) {
                    $group->links()->create([
                        'title'    => 'تست',
                        'link'     => '#',
                        'ordering' => $i,
                    ]);
                }
            }
        }
    }
}
