<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            // post permissions
            [
                'name'  => 'create posts',
                'label' => 'ایجاد نوشته'
            ],
            [
                'name'  => 'update posts',
                'label' => ' ویرایش نوشته ها'
            ],
            [
                'name'  => 'viewAny posts',
                'label' => 'لیست نوشته ها'
            ],
            [
                'name'  => 'delete posts',
                'label' => 'حذف نوشته ها'
            ],

        ];

        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
    }
}
