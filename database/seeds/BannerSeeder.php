<?php

use App\Banner;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(!Storage::disk('public')->exists('uploads/banners/1.jpg')) {
            Storage::disk('public')->copy('front/img/banner/medium-banner-1.jpg', 'uploads/banners/1.jpg');
        }
        
        Banner::create([
            'link'        => '#',
            'group'       => 1,
            'published'   => true,
            'image'       => '/uploads/banners/1.jpg',
            'lang'        => app()->getLocale(),
        ]);

        if(!Storage::disk('public')->exists('uploads/banners/2.jpg')) {
            Storage::disk('public')->copy('front/img/banner/medium-banner-2.jpg', 'uploads/banners/2.jpg');
        }
        
        Banner::create([
            'link'        => '#',
            'group'       => 1,
            'published'   => true,
            'image'       => '/uploads/banners/2.jpg',
            'lang'        => app()->getLocale(),
        ]);

        if(!Storage::disk('public')->exists('uploads/banners/3.jpg')) {
            Storage::disk('public')->copy('front/img/banner/sidebar-banner-1.gif', 'uploads/banners/3.jpg');
        }
        
        Banner::create([
            'link'        => '#',
            'group'       => 2,
            'published'   => true,
            'image'       => '/uploads/banners/3.jpg',
            'lang'        => app()->getLocale(),
        ]);
    }
}
