<?php

use App\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ordering = 1;

        foreach (get_langs() as $lang => $info) {
            Menu::create([
                'title'       => 'صفحه اصلی',
                'type'        => 'normal',
                'link'        => '/',
                'ordering'    => $ordering++,
                'lang'        => $lang,
            ]);


            Menu::create([
                'title'       => 'وبلاگ',
                'type'        => 'static',
                'static_type' => 'posts',
                'ordering'    => $ordering++,
                'lang'        => $lang,
            ]);

            Menu::create([
                'title'       => 'تماس با ما',
                'type'        => 'normal',
                'link'        => '/contact',
                'ordering'    => $ordering++,
                'lang'        => $lang,
            ]);
        }
    }
}
